#!/usr/bin/env python
#
# Copyright (c) 2002-2004 Juri Pakaste <juri@iki.fi>
# Copyright (c) 2005-2007 Straw Contributors 
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


# wrap stdout to print unicode chars in the $TERM
import codecs, sys
sys.stdout = codecs.lookup('utf-8')[-1](sys.stdout)

import signal, os, os.path, time
import pygtk
pygtk.require("2.0")
import gtk, gtk.glade


def initialize_gettext():
    import gettext, locale
    from straw import defs

    appname = defs.PACKAGE
    localedir = "%s/%s" % (defs.DATA_DIR,'locale')

    gettext.bindtextdomain(appname, localedir)
    gettext.bind_textdomain_codeset(appname, 'UTF-8')
    gettext.textdomain(appname)
    gettext.install(appname, localedir, unicode=1)

    locale.bindtextdomain(appname, localedir)
    locale.bind_textdomain_codeset(appname, localedir)
    locale.textdomain(appname)

    gtk.glade.bindtextdomain(appname, localedir)
    return

def get_root():
    dir = os.path.dirname (os.path.abspath
                   (sys._getframe().f_code.co_filename))
    rootdir = os.path.abspath(os.path.join(dir, ".."))
    return rootdir


def get_library_location():
    source = None
    if os.environ.has_key('STRAW_IN_SOURCE_DIR'):
        source = os.path.join(get_root(), 'straw')
    elif os.environ.has_key("STRAW_LIB"):
        source = os.environ['STRAW_LIB']
    else:
        for d in sys.path:
            sd = os.path.join(d, 'straw')
            if os.path.isdir(sd):
                source = sd
    if not source:
        raise "FileNotFoundError", "couldn't find straw library dir"
    return source

def setup():
    """
    Run straw in the source tree
    """
    print 'Generating constants.py...'
    libloc = get_library_location()
    rootdir = get_root()
    f = open (os.path.join(libloc,'constants.py'),'w')
    f.write('APPNAME = "Straw"\n')
    f.write('VERSION = "0.28-dev"\n')
    f.write('STRAW_URL = "http://www.gnome.org/projects/straw"\n')
    f.write('STRAW_REF_URL = "http://www.gnome.org/projects/straw-ref.html"\n')
    f.write('dataroot  = "%s"\n' % os.path.abspath(rootdir))
    f.close()

    # insert straw module into python path
    sys.path.insert(0, rootdir)
    print 'done'

def tear_down():
    # remove created compiled files
    import dircache
    libloc = get_library_location()
    httplibdir = os.path.join(libloc, "httplib2")
    for d in [libloc, httplibdir]:
        for fn in dircache.listdir(d):
            filepath = os.path.join(d, fn)
            if filepath.endswith(".pyc"):
                os.remove(filepath)
    os.remove(os.path.join(libloc, 'constants.py'))
    print '.. exiting.'


def parse_args():
    from optparse import OptionParser
    usage = """%prog [--offline]\nDesktop feed aggregator"""
    parser = OptionParser(usage=usage)
    parser.set_defaults(offline=False)
    parser.add_option("--offline", action="store_true", dest="offline", default=False,
                      help="run Straw in offline mode")
    (options,args) = parser.parse_args()
    return options

def run(options):
    from straw import Application, Config, Constants

    # set offline to false if offline. Previous releases just rely on the
    # previous state of the 'offline' config. So if the user specifies
    # offline, the next run will still be set to offline regardless if the
    # user did not specifiy offline since it will still get the state of the
    # offline config from the previous run.
    offline = Config.set(Constants.OPTION_OFFLINE, options.offline)

    app = Application.Application()

    app.mainloop()

def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    options = parse_args()
    in_source = os.environ.has_key("STRAW_IN_SOURCE_DIR")
    prof = os.environ.has_key('STRAW_PROFILE')

    if in_source:
        setup()

    if prof:
        from tools import statprof
        statprof.start()

    initialize_gettext()
    run(options)

    if prof:
        statprof.stop()
        statprof.display()

    if in_source:
        tear_down()

if __name__ == '__main__':
    main()
