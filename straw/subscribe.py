""" subscribe.py

Module for handling feed subscription process.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """ GNU General Public License

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Constants import *
from gtk.glade import XML
import Config
import FeedDiscovery
import FeedManager
import MVP
import SummaryParser
import error
import gettext
import gobject
import gtk
import helpers
import os, os.path
import pygtk
import straw.defs
import time
import urllib
import urlparse
import xml
pygtk.require('2.0')

STATE_INTRO = 1
STATE_FINISH = 2

class SubscribeView(MVP.GladeView):
    def _initialize(self):
        self._window         = self._widget.get_widget('subscribe_dialog')
        self._button1        = self._widget.get_widget('button1')
        self._button2        = self._widget.get_widget('button2')
        self._button3        = self._widget.get_widget('button3')
        self._progress_bar   = self._widget.get_widget('progress_bar')
        self._notebook       = self._widget.get_widget('notebook')
        self._location_entry = self._widget.get_widget('feed_location_entry')
        self._username_entry = self._widget.get_widget('username_entry')
        self._password_entry = self._widget.get_widget('password_entry')
        self._category_tree  = self._widget.get_widget('category_tree')
        self._error_text     = self._widget.get_widget('error_text')
        self._error_box      = self._widget.get_widget('error_box')
        self._result_tree    = self._widget.get_widget('result_tree')

        self._result_size_label = self._widget.get_widget('result_size_label')
        self._result_size_text = self._result_size_label.get_text()

        self.category_pixbuf = gtk.Label().render_icon(gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_MENU)

        self.feeds = []

    def _populate_tree(self, parent_id, parent = None):
        if not self.nodes.has_key(parent_id):
            return

        for node in self.nodes[parent_id].children:
            if node.type == "C":
                treeiter = self.category_store.append(parent, [self.category_pixbuf, helpers.pango_escape(node.name), node])

                if self.nodes.has_key(node.id):
                    self._populate_tree(node.id, treeiter)

    def _on_forward(self):
        location = self._location_entry.get_text()
        if not location:
            self.display_error(_("Feed Location must not be empty"))
            return False
        #location = "file:///home/ppawel/Desktop/test.rss"
        username = self._username_entry.get_text()
        username = username.strip()
        password = self._password_entry.get_text()
        password = password.strip()

        if Config.get(OPTION_OFFLINE):
            self._window.hide()
            response = helpers.report_offline_status(self._window)

            if response == gtk.RESPONSE_CANCEL:
                self.show()
                return False

            Config.set(OPTION_OFFLINE, True)

        credentials = None

        if username or password:
            credentials = username, password, ""

        self.feeds = []

        observers = [ { "job-done": [ self._discovery_finished ],
                       "feed-discovered": [ self._on_feed_discovered ] } ]

        FeedDiscovery.discover(location, credentials, observers)

        return True

    def progress_pulse(self, obj):
        self._progress_bar.pulse()
        return True

    def progress_destroy(self):
        if self.timer != 0:
            gobject.source_remove(self.timer)
            self.timer = 0

    def _setup_entries(self):
        self._location_entry.set_text("http://")
        self._username_entry.delete_text(0,-1)
        self._password_entry.delete_text(0,-1)

        self._location_entry.grab_focus()

    def show(self):
        self.state = STATE_INTRO
        self.set_state(gtk.STOCK_CLOSE, gtk.STOCK_GO_FORWARD, None)
        self.timer = 0

        self._setup_entries()
        self._error_box.set_property('visible',False)
        self._notebook.set_current_page(0)
        self._window.show()

        self.store = gtk.ListStore(bool, str, gobject.TYPE_PYOBJECT)

        renderer = gtk.CellRendererToggle()
        renderer.set_property("activatable", True)
        column = gtk.TreeViewColumn(_('Keep'), renderer, active = 0)
        renderer.connect('toggled', self.col1_toggled_cb, self.store)
        self._result_tree.append_column(column)

        renderer2 = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('_Title'), renderer2, markup = 1)

        self._result_tree.append_column(column)
        self._result_tree.set_model(self.store)
        
        column = gtk.TreeViewColumn()

        status_renderer = gtk.CellRendererPixbuf()
        column.pack_start(status_renderer, False)
        column.set_attributes(status_renderer, pixbuf = 0)

        title_renderer = gtk.CellRendererText()
        column.pack_start(title_renderer, False)
        column.set_attributes(title_renderer, markup = 1)

        self._category_tree.append_column(column)

    def col1_toggled_cb(self, cell, path, model):
        self.store[path][0] = not self.store[path][0]

        # Enable "Apply" button when at least one discovered feed is selected.

        self._button3.set_sensitive(bool(sum([feed[0] for feed in self.store])))

    def setup_category_tree(self):
        self.category_store = gtk.TreeStore(gtk.gdk.Pixbuf, str, gobject.TYPE_PYOBJECT)
        self.nodes = FeedManager.get_model()

        self._category_tree.set_model(None)
        self._category_tree.set_model(self.category_store)

        treeiter = self.category_store.append(None, [self.category_pixbuf, "Main category", None])

        self._populate_tree(1, parent = treeiter)
        self._category_tree.expand_all()

        selection = self._category_tree.get_selection()
        selection.select_path("0")

    def display_error(self, text):
        self._error_box.set_property('visible',True)
        self._error_text.set_text(text)

    def set_location_entry(self, location):
        self._location_entry.set_text(location)

    def set_parent(self, parent):
        self._window.set_transient_for(parent)

    def _on_button1_clicked(self, *args):
        self.progress_destroy()
        self._window.hide()

    def _on_button2_clicked(self, *args):
        if self.state == STATE_INTRO:
            if self._on_forward():
                self._progress_bar.props.visible = True
                self.timer = gobject.timeout_add(100, self.progress_pulse, self)
                self._button2.set_sensitive(False)
        elif self.state == STATE_FINISH:
            self.state = STATE_INTRO
            self.set_state(gtk.STOCK_CLOSE, gtk.STOCK_GO_FORWARD, None)
            self._notebook.set_current_page(0)

    def _on_button3_clicked(self, *args):
        if self.state == STATE_FINISH:
            new_feeds = []

            # Obtain selected category.

            parent = None
            iter = self._category_tree.get_selection().get_selected()[1]
            
            if iter != None:
                path = self.category_store.get_path(iter)
                parent = self.category_store[path][2]

            # Save selected feeds.

            for feed in [row[2] for row in self.store if row[0]]:
                feed.parent = parent
                new_feeds.append(feed)

            FeedManager.save_all(new_feeds)

            self._window.hide()

    def _on_feed_discovered(self, handler, feed):
        self.feeds.append(feed)

    def _discovery_finished(self, handler, task_result):
        gtk.gdk.threads_enter()
        feeds = self.feeds

        self._result_size_label.set_text(self._result_size_text % len(feeds))

        for feed in feeds:
            label = helpers.pango_escape(feed.title) + '\n<span size="smaller">' + \
                helpers.pango_escape(feed.location) + '</span>'

            self.store.append([None, label, feed])

        self._progress_bar.props.visible = False
        self._button2.set_sensitive(True)
        self._button3.set_sensitive(False)
        self.progress_destroy()
        self.state = STATE_FINISH
        self.set_state(gtk.STOCK_CLOSE, gtk.STOCK_GO_BACK, gtk.STOCK_APPLY)
        self._notebook.set_current_page(1)

        self.setup_category_tree()

        gtk.gdk.threads_leave()

    def set_state(self, b1, b2, b3):
        if b1 == None:
            self._button1.hide()
        else:
            self._button1.set_label(b1)
            self._button1.show()
        
        if b2 == None:
            self._button2.hide()
        else:
            self._button2.set_label(b2)
            self._button2.show()
        
        if b3 == None:
            self._button3.hide()
        else:
            self._button3.set_label(b3)
            self._button3.show()

    def _on_feed_location_entry_key_press_event(self, widget, event):
        if event.keyval == gtk.keysyms.Return:
            self._presenter.subscribe(self._location_entry.get_text())

class SubscribePresenter(MVP.BasicPresenter):

    def _normalize(self, url):
        u = urlparse.urlsplit(url.strip())
        # we check if 'scheme' is not empty here because URIs like
        # "google.com" IS valid but, in this case, 'scheme' is empty because
        # urlsplit() expects urls are in the format of scheme://netloc/...
        if not u[0] or (u[0] != "http" and u[0] != "feed"):
            return None
        if u[0] == 'feed':
            u = urlparse.urlsplit(u[2])
        # .. if that happens then we munge the url by adding a // and assume
        # that 'http' is the scheme.
        if u[1] == '':
            u = urlparse.urlsplit("//" + url, 'http')
        return u

    def subscribe(self, location, username=None, password=None):
        self._username = username
        self._password = password
        self._location = location
        
        import FeedManager
        import model

        feed = model.Feed()
        feed.location = location
        feed.title = location
        FeedManager.save_feed(feed)

    def set_parent(self, parent):
        self.view.set_parent(parent)

    def get_credential(self):
        return (self._location, self._username, self._password)

    def show(self, url=None):
        if url:
            self.view.set_location_entry(url)
        self.view.show()

#    def set_url_dnd(self, url):
#        self._window.show()
#        self._feed_location_presenter.view.find_feed(url)

    def set_location_from_clipboard(self):
        def _clipboard_cb(cboard, url, data=None):
            if url:
                url = self._normalize(url)
                if url and url[0] == "http":
                    url = urlparse.urlunsplit(url)
                    self.view.set_location_entry(url)
        clipboard = gtk.clipboard_get(selection="CLIPBOARD")
        clipboard.request_text(_clipboard_cb, None)

def subscribe_show(url=None, parent=None):
    gladefile = XML(os.path.join(straw.defs.STRAW_DATA_DIR, "subscribe.glade"))
    _dialog = SubscribePresenter(view=SubscribeView(gladefile))
    _dialog.set_parent(parent)
    _dialog.set_location_from_clipboard()
    _dialog.show()
