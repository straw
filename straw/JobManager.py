""" JobManager.py

Provides flexible infrastructure for handling various jobs from within the
application.

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from straw.Queue import Queue, Empty # our copy of the python 2.5 version
from gobject import GObject, SIGNAL_RUN_FIRST
from threading import Thread
import error
import gobject
import time

LOG_STRING_LIMIT = 50

class JobManager(GObject):
    """
    Main entry point for the subsystem. JobManager is responsible managing
    JobHandlers, starting/stopping jobs.

    """

    def __init__(self):
        self.handlers = {}
        self.active = {}

    def _get_id(self):
        return time.time()

    def register_handler(self, clazz):
        self.handlers[clazz.job_id] = clazz

    def start(self, job, wait = False):
        id = self._get_id()
        handler = self.handlers[job.job_id](id, job)
        handler_thread = HandlerThread(handler)

        handler.connect("cleanup", self._on_cleanup)
        self.active[id] = handler_thread
        handler_thread.start()

        if wait:
            handler_thread.join()

        return id

    def stop(self, id):
        self.active[id].stop()

    def stop_jobs(self):
        for handler_thread in self.active.values():
            handler_thread.stop()

    def _on_cleanup(self, handler, data):
        del self.active[handler.id]

class Job(object):
    def __init__(self, job_id):
        self.job_id = job_id
        self.tasks = None
        self.observers = None

class JobHandler(GObject):
    """
    This class represents an abstract JobHandler which provides common functionality
    used by all handlers. Each job handler is created with given Job to do.
    
    JobHandler implementation should be aware of job's specifics, ie. should be able
    to split a job into multiple tasks.
    
    It is also responsible for providing notification facilities for workers.

    """

    __gsignals__ = {
        'job-done' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'task-done' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'task-start' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'cleanup' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
    }

    def __init__(self, id, job):
        gobject.GObject.__init__(self)
        self.id = id
        self.job = job
        self.task_queue = Queue()
        self.result_queue = Queue()
        self.job_size = None

        if job.observers != None:
            for observer_dict in job.observers:
                for signal in observer_dict.keys():
                    for callable in observer_dict[signal]:
                        self.connect(signal, callable)

    def _debug(self, msg):
        error.debug("%s %s" % (self.job_id, msg))

    def start(self):
        self._prepare()
        self._run()
        self._shutdown()
        self._cleanup()

    def stop(self):
        raise NotImplementedError

    def _prepare(self):
        pass

    def _run(self):
        raise NotImplementedError

    def _shutdown(self):
        self._debug("shutting down...")
        self._notify("job-done")

    def _cleanup(self):
        self._notify("cleanup")

    def _notify(self, event, data = None):
        self.emit(event, data)

    def _post_result(self, task, result):
        self.result_queue.put(result)

class HandlerThread(Thread):
    def __init__(self, handler):
        Thread.__init__(self)

        self.handler = handler

    def run(self):
        self.handler.start()

    def stop(self):
        self.handler.stop()

class ThreadPoolJobHandler(JobHandler):
    """
    This handler uses a thread pool to efficiently process the data in
    non-blocking manner. Useful for jobs that need to be done in the
    background.

    """

    def __init__(self, id, job):
        JobHandler.__init__(self, id, job)

        self.pool_size = 3
        self.running_queue = Queue()
        self.task_threads = []
        self.task_class = None

    def _run(self):
        if not self.job_size:
            self.job_size = self.task_queue.qsize()

        for i in xrange(self.pool_size):
            t = self.task_class(self)
            t.setName(str(i))
            t.start()
            self.task_threads.append(t)

        self._debug("created %d thread(s), waits for results" % len(self.task_threads))

        for t in xrange(self.job_size):
            self._debug("waiting...")
            task_result = self.result_queue.get()
            self._debug("got result [%s]" % str(task_result))
            
            if task_result:
                self._notify("task-done", task_result)
            else:
                self._debug("stopping...")
                break

        self._debug("got the job done, waits for %d worker(s)" % len(self.task_threads))

        for t in self.task_threads:
            self.task_queue.put("job-done")
            self._debug("put job-done")

        self.running_queue.join()

    def stop(self):
        for t in self.task_threads:
            t.job_done()

class TaskResult:
    def __init__(self, task, result):
        self.task = task
        self.result = result

class TaskThread(Thread):
    def __init__(self, handler):
        self.handler = handler
        self.task_queue = handler.task_queue
        self.result_queue = handler.result_queue
        self.running_queue = handler.running_queue
        self.quit = False

        Thread.__init__(self)

    def _debug(self, msg):
        error.debug("%s-%s %s" % (self.handler.job_id, self.getName(), msg))

    def run(self):
        self.running_queue.put(True)

        while 1:
            self._debug("waiting to get a task")

            task = self.task_queue.get()

            self._debug("got a task [%s]" % str(task)[:LOG_STRING_LIMIT])

            if self.quit or task == "job-done":
                self._debug("breaking")
                self._post_result("job-done", None)
                break

            if task:
                self._debug("munching task [%s]" % str(task)[:LOG_STRING_LIMIT])
                self._task_started(task)
                result = None

                try:
                    result = self._process(task)
                except Exception, e:
                    self._debug("has thrown an exception while processing task [%s] [e = %s]" % (str(task)[:LOG_STRING_LIMIT], str(e)))
                    #error.log_exc("Details:")

                self._post_result(task, result)

        self.running_queue.get()
        self.running_queue.task_done()
        self._debug("terminating, bye")

    def job_done(self):
        self._debug("got job_done")
        self.quit = True

    def _post_result(self, task, result):
        if task == "job-done":
            self.result_queue.put(None)
            self._debug("posted None")
        else:
            self._debug("posting result to task [%s]" % str(task)[:LOG_STRING_LIMIT])

            task_result = TaskResult(task, result)
            self.result_queue.put(task_result)
            self.task_queue.task_done()

    def _prepare_task(self, task):
        return task

    def _task_started(self, task):
        self.handler._notify("task-start", self._prepare_task(task))

    def _process(self, task):
        raise NotImplementedError

_job_manager = None

def _get_instance():
    global _job_manager
    if not _job_manager:
        _job_manager = JobManager()
    return _job_manager

def register_handler(clazz):
    _get_instance().register_handler(clazz)

def start(job, wait = False):
    return _get_instance().start(job, wait)

def stop(id):
    _get_instance().stop(id)

def stop_jobs():
    _get_instance().stop_jobs()
