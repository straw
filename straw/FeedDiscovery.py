"""

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from JobManager import Job, TaskResult, TaskThread, JobHandler
import Fetcher
import JobManager
import SummaryParser
import feedfinder
import gobject

class FeedDiscoveryJobHandler(JobHandler):
    job_id = "feed-discovery"

    __gsignals__ = {
        "feed-discovered" : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
    }

    def __init__(self, id, job):
        JobHandler.__init__(self, id, job)

    def _on_fetch_started(self, handler, task):
        pass

    def _on_url_fetched(self, handler, task_result):
        url = task_result.task.user_data
        fetch_result = task_result.result

        if feedfinder.couldBeFeedData(fetch_result.content):
            feed = SummaryParser.parse(fetch_result.content, location = url)
            self._notify("feed-discovered", feed)

    def _on_fetch_done(self, handler, data):
        pass

    def _prepare(self):
        self.observers = [{ "task-done": [ self._on_url_fetched ],
                      "task-start": [ self._on_fetch_started ],
                      "job-done": [ self._on_fetch_done ]}]        

    def _run(self):
        fetch_task = Fetcher.create_task(url = self.job.url, credentials = self.job.credentials, user_data = None)
        fetch_result = fetch_task.fetch()

        if fetch_result.error:
            return

        if feedfinder.couldBeFeedData(fetch_result.content):
            feed = SummaryParser.parse(fetch_result.content, location = self.job.url)

            if self.job.credentials:
                print self.job.credentials
                feed.pdict["username"], feed.pdict["password"], domain = self.job.credentials

            self._notify("feed-discovered", feed)
            return

        urls = feedfinder.urls(self.job.url, fetch_result.content)
        fetch_tasks = [Fetcher.create_task(url = url, user_data = url) for url in urls]
        self.fetcher_id = Fetcher.fetch(fetch_tasks, observers = self.observers, wait = True)

class FeedDiscoveryJob(Job):
    def __init__(self, url, credentials, observers):
        Job.__init__(self, "feed-discovery")

        self.observers = observers
        self.url = url
        self.credentials = credentials

JobManager.register_handler(FeedDiscoveryJobHandler)

def discover(url, credentials, observers):
    job = FeedDiscoveryJob(url, credentials, observers)
    JobManager.start(job)
