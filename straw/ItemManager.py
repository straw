from error import debug
from gobject import GObject, SIGNAL_RUN_FIRST
from model import Feed, Item, Category
from storage import DAO, Storage
import FeedManager
import Queue
import gobject
import threading

_storage_path = None
model_data = None
calls = Queue.Queue()

class ServiceThread(threading.Thread):
    def run(self):
        global calls

        while 1:
            call = calls.get()

            if not call:
                break

            result = call[0](call[1][0])
            gobject.idle_add(call[2], result)

serviceThread = ServiceThread()
serviceThread.start()

def init():
    fm = _get_instance()
    fm.init_storage(_storage_path)

def setup(storage_path = None):
    global _storage_path
    _storage_path = storage_path

def shutdown():
    calls.put(None)
    serviceThread.join()

def get_feed_items(id):
    im = _get_instance()
    return im.get_feed_items(id)

def get_category_items(id, callback):
    global calls

    im = _get_instance()
    calls.put_nowait((im.get_category_items, (id,), callback))
    #print calls.get_nowait()
    #return im.get_category_items(id)
    return []

def feed_item_exists(item):
    im = _get_instance()
    return im.feed_item_exists(item)

class ItemManager(GObject):
    __gsignals__ = {
        "item-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "feed-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "category-added" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "update-all-done" :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }

    def __init__(self):
        GObject.__init__(self)

        self.storage = None

    def init_storage(self, path):
        self.storage = Storage(path)
        self.dao = DAO(self.storage)

    def get_category_items(self, id):
        category = FeedManager.lookup_category(id)

        if not category:
            return None

        in_str = ", ".join([str(feed.id) for feed in FeedManager.get_children_feeds(id)])

        query = "SELECT id, title, feed_id, is_read, link, pub_date FROM items WHERE feed_id IN(%s) ORDER BY pub_date DESC" % in_str
        #query = "SELECT id, title, feed_id, is_read, link, pub_date FROM items WHERE feed_id IN(%s) ORDER BY pub_date DESC" % in_str# LIMIT %d OFFSET %d" % (in_str, size, start)
        #query = "SELECT id, title, feed_id, is_read, link, pub_date FROM items ORDER BY pub_date DESC"# LIMIT %d OFFSET %d" % (in_str, size, start)

        items = self.dao.get(Item, query = query)

        for item in items:
            feed = FeedManager.lookup_feed(item.feed_id)
            item.feed = feed
            item.connect("is-read-changed", feed.on_is_read_changed)
            item.connect("is-read-changed", self.on_item_is_read_changed)

        return items

    def get_feed_items(self, id):
        feed = FeedManager.lookup_feed(id)
        feed.items = self.dao.get(Item, " WHERE feed_id = %d ORDER BY pub_date DESC" % feed.id)

        for item in feed.items:
            item.feed = feed
            item.connect("is-read-changed", feed.on_is_read_changed)
            item.connect("is-read-changed", self.on_item_is_read_changed)

        return feed.items

    def feed_item_exists(self, item):
        result = self.dao.get(Item, sql = " WHERE feed_id = ? AND title = ?", params = (str(item.feed_id), str(item.title),))
        return len(result) > 0

    def on_item_is_read_changed(self, obj, is_read):
        self.dao.save(obj)

_instance = None

def _get_instance():
    global _instance
    if not _instance:
        _instance = ItemManager()
    return _instance
