""" dialogs.py

Module for displaying dialogs related to errors, warnings, notifications,
etc...
"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Constants import *
from gettext import gettext as _
import Config
import codecs
import error
import gnomevfs
import gobject
import gtk
import htmlentitydefs
import opml
import os.path, re, locale, time, calendar
import straw
import urllib, string, urlparse
import xml

log = error.get_logger()

class UIFactory(object):
    _shared_state = {}
    manager = None

    actions = [
        ("refresh", gtk.STOCK_REFRESH, _("_Refresh"), None, _("Update this feed"), None),
        ("add_child", gtk.STOCK_ADD, _("Add child"), None, _("Update this feed"), None),
        ("mark_as_read", gtk.STOCK_CLEAR, _("_Mark As Read"),
         None, _("Mark all items in this feed as read"), None),
        ("stop_refresh", None, _("_Stop Refresh"), None, _("Stop updating this feed"), None),
        ("remove", gtk.STOCK_REMOVE, _("Remo_ve"), None, _("Remove"), None),
        ("properties", gtk.STOCK_INFO, _("_Information"), None, _("Feed-specific properties"), None),
        ("text_copy", gtk.STOCK_COPY, "_Copy", None, None, None),
        ("link_copy", None, "_Copy Link Location", None, None, None),
        ("link_open", None, "_Open Link", None, None, None),
        ("subscribe", None, "_Subscribe", None, None, None),
        ("zoom_in", gtk.STOCK_ZOOM_IN, "Zoom _In", None, None, None),
        ("zoom_out", gtk.STOCK_ZOOM_OUT, "Zoom _Out", None, None, None),
        ("zoom_100", gtk.STOCK_ZOOM_100, "_Normal Size", None, None, None),
        ("mark_as_unread", gtk.STOCK_CONVERT, "Mark as _Unread", None, "Mark this item as unread", None)
        ]

    def __new__(cls, *a, **k):
        obj = object.__new__(cls, *a, **k)
        obj.__dict__ = cls._shared_state
        return obj

    def __init__(self, name):
        if not self.manager:
            log.info("Initializing UI manager")
            self.action_groups = {}
            self.manager = gtk.UIManager()
            action_group = gtk.ActionGroup(name)
            action_group.add_actions(UIFactory.actions)
            num_action_groups = len(self.manager.get_action_groups())
            self.action_groups[name] = num_action_groups
            self.manager.insert_action_group(action_group, num_action_groups)
            ui = os.path.join(straw.defs.STRAW_DATA_DIR, 'ui.xml')
            self.manager.add_ui_from_file(ui)

    def get_popup(self, path):
        return self.manager.get_widget(path)

    def get_action(self, path):
        return self.manager.get_action(path)

    def ensure_update(self):
        self.manager.ensure_update()


def report_error(primary, secondary, parent=None):
    dialog = gtk.MessageDialog(parent,
                               type=gtk.MESSAGE_ERROR,
                               buttons=gtk.BUTTONS_OK,
                               message_format=primary)
    dialog.format_secondary_text(secondary)
    response = dialog.run()
    dialog.destroy()
    return response

def report_offline_status(parent=None):
    dialog = gtk.MessageDialog(parent,
                               type=gtk.MESSAGE_WARNING,
                               buttons=gtk.BUTTONS_OK_CANCEL,
                               message_format="You Are Offline!")
    dialog.format_secondary_markup(_("You are currently reading offline. Would you like to go online now?"))
    response = dialog.run()
    dialog.destroy()
    return response

def credits():
    people = u'''Author:
Juri Pakaste <juri@iki.fi>

Contributors:
Jan Alonzo <jmalonzo@unpluggable.com>
Ryan P. Skadberg <skadz@stigmata.org>
Leandro Lameiro <lameiro@gmail.com>
Tuukka Hastrup <tuukka@iki.fi>

Past Contributors:
Iain McCoy <iain@mccoy.id.au>
Lucas Nussbaum <lucas@lucas-nussbaum.net>
Olivier Crete <tester@tester.ca>
Scott Douglas-Watson <sdouglaswatson@yahoo.co.uk>
Terje R\xf8sten (distutils)

Special Thanks:
Mark Pilgrim (feedparser and feedfinder)
Joe Gregorio (httplib2)
'''
    iconfile = os.path.join(straw.defs.STRAW_DATA_DIR,"straw.svg")
    logo = gtk.gdk.pixbuf_new_from_file(iconfile)
    description = _("A Desktop Feed Reader")
    straw_copyright = u"""
    Copyright \xa9 2005-2007 Straw Contributors
    Copyright \xa9 2002-2004 Juri Pakaste"""
    artists = [
        u'Jakub \'jimmac\' Steiner',
        u'Juri Pakaste'
        ]

    translators = [
        u"GNOME i18n Project and Translators<http://developer.gnome.org/projects/gtp/>",
        u"Juri Pakaste <juri@iki.fi>",
        u"Martin Steldinger <tribble@hanfplantage.de>",
        u"David Rousseau <boiteaflood@wanadoo.fr>",
        u"Sergei Vavinov <svv@cmc.msu.ru>",
        u"Terje R\xf8sten <terjeros@phys.ntnu.no>",
        u"Francisco J. Fernandez <franciscojavier.fernandez.serrador@hispalinux.es>",
        u"Elros Cyriatan (Dutch Translation)"
        ]

    translator_credits = 'translator_credits'
    about = gtk.AboutDialog()
    about.set_name(straw.defs.PACKAGE)
    about.set_version(straw.defs.VERSION)
    about.set_copyright(straw_copyright)
    about.set_comments(description)
    about.set_authors(people.splitlines())
    about.set_artists(artists)
    about.set_logo(logo)
    about.set_translator_credits(translator_credits)
    about.set_license(__license__)
    gtk.about_dialog_set_url_hook(lambda about, url: url_show(url))
    about.set_website(straw.defs.STRAW_HOME)
    about.set_website_label(straw.defs.STRAW_HOME)
    about.connect('response', lambda widget, response: widget.destroy())
    return about

def listdiff(l1, l2, test=None):
    if test is not None:
        return _listdifftest(l1, l2, test)
    common = []
    inl1 = []
    inl2 = []
    for e in l1:
        if e in l2:
            common.append(e)
        else:
            inl1.append(e)
    for e in l2:
        if e in l1:
            if e not in common:
                common.append(e)
        else:
            inl2.append(e)
    return (common, inl1, inl2)

def url_show(url):
    browser_type = Config.get(OPTION_WEB_BROWSER_TYPE)

    if browser_type == "gnome":
        return gnomevfs.url_show(url)
    else:
        try:
            browser_cmd = Config.get(OPTION_WEB_BROWSER_CMD)
            cmdbin, args = string.splitfields(browser_cmd, maxsplit=1)
            link = args % url
            import subprocess
            pid = subprocess.Popen([cmdbin, link]).pid
            return pid
        except ValueError, ve:
            raise UrlOpenException("Problem opening link %s" % ve)
        

def set_clipboard_text(text):
    clipboard = gtk.clipboard_get(selection="CLIPBOARD")
    clipboard.set_text(text)


class UrlOpenException(Exception):
    pass

entity = re.compile(r'\&.\w*?\;')
def convert_entities(text):
    def conv(ents):
        entities = htmlentitydefs.entitydefs
        ents = ents.group(0)
        ent_code = entities.get(ents[1:-1], None)
        if ent_code is not None:
            try:
                ents = unicode(ent_code, get_locale_encoding())
            except UnicodeDecodeError:
                ents = unicode(ent_code, 'latin-1')
            except Exception, ex:
                error.log("error occurred while converting entity %s: %s" % (ents, ex))

            # check if it still needs conversion
            if entity.search(ents) is None:
                return ents

        if ents[1] == '#':
            code = ents[2:-1]
            base = 10
            if code[0] == 'x':
                code = code[1:]
                base = 16
            return unichr(int(code, base))
        else:
            return

    in_entity = entity.search(text)
    if in_entity is None:
        return text
    else:
        ctext = in_entity.re.sub(conv, text)
        return ctext

def complete_url(url, feed_location):
    #print "---- COMPLETING %s WITH %s" % (url, feed_location)
    url = urllib.quote(url, safe=string.punctuation)

    if urlparse.urlparse(url)[0] == '':
        return urlparse.urljoin(feed_location, url)
    else:
        return url

def get_url_location(url):
    url = urllib.quote(url, safe=string.punctuation)
    parsed_url = urlparse.urlsplit(url)
    return urlparse.urlunsplit((parsed_url[0], parsed_url[1], '','',''))

def get_locale_encoding():
    try:
        encoding = locale.getpreferredencoding()
    except locale.Error:
        encoding = sys.getdefaultencoding()
    return encoding

def format_date(date, format=None, encoding=None):
    return date
    if format is None:
        format = get_date_format()
    if encoding is None:
        encoding = get_locale_encoding()
    timestr = time.strftime(format, time.localtime(calendar.timegm(date)))
    return unicode(timestr, encoding)

def get_date_format():
    # this is here just to make xgettext happy: it should be defined in
    # only one place, and a good one would be MainWindow.py module level.
    # however, we can't access _ there.
    # The format: %A is the full weekday name
    #             %B is the full month name
    #             %e is the day of the month as a decimal number,
    #                without leading zero
    # This should be translated to be suitable for the locale in
    # question, feel free to alter the order and the parameters (they
    # are strftime(3) parameters, the whole string is passed to the
    # function, Straw does no additional interpretation) if you feel
    # it's necessary in the translation file.
    return _('%A %B %e %H:%M')

def pango_escape(text):
    if text:
        return gobject.markup_escape_text(text)#xml.sax.saxutils.escape(text)
    else:
        return ""

def html_replace(exc):
    """ Python Cookbook 2ed, Section 1.23
    """
    if isinstance(exc, (UnicodeDecodeError, UnicodeTranslateError)):
        s = [ u'&%s;' % codepoint2name[ord(c)] for c in exc.object[exc.start:exc.end]]
        return ''.join(s), exc.end
    else:
        raise TypeError("can't handle %s" % exc.__name__)

codecs.register_error('html_replace', html_replace)
