""" Fetcher.py

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from JobManager import Job, TaskResult, TaskThread, ThreadPoolJobHandler
from threading import Semaphore
import Config
import JobManager
import httplib2
import os
import urlparse

def _get_url_schema(url):
    return urlparse.urlsplit(url, "http")[0]

class Fetcher(object):
    def __init__(self):
        pass

    def supports(self, **kwargs):
        raise NotImplementedError

class FetchTask(object):
    def __init__(self, fetcher, url, user_data):
        self.fetcher = fetcher
        self.url = url
        self.user_data = user_data

    def __str__(self):
        return "[fetch: url = (%s)]" % self.url

    def fetch(self):
        return self.fetcher.fetch(self)

class FetchResult(object):
    def __init__(self, content, error):
        self.content = content
        self.error = error

class HttpFetcher(Fetcher):
    def __init__(self):
        Fetcher.__init__(self)

    def supports(self, **kwargs):
        schema = _get_url_schema(kwargs["url"])
        return schema == "http" or schema == "https"

    def create_task(self, **kwargs):
        return HttpFetchTask(self, **kwargs)

    def fetch(self, task):
        url = task.url
        CACHE_DIR = os.path.join(Config.straw_home(), 'cache')
        http = httplib2.Http(CACHE_DIR)

        content = None
        error = None

        if task.credentials:
            username, password, domain = task.credentials
            http.add_credentials(username, password, domain)

        try:
            response, content = http.request(url, "GET")
        except Exception, e:
            error = e

        if not error and response.status >= 400:
            error = response

        return FetchResult(content, error)

class HttpFetchTask(FetchTask):
    def __init__(self, fetcher, **kwargs):
        FetchTask.__init__(self, fetcher, kwargs["url"], kwargs["user_data"])

        self.credentials = None

        if kwargs.has_key("credentials"):
            self.credentials = kwargs["credentials"]

class FileFetchTask(FetchTask):
    def __init__(self, fetcher, **kwargs):
        FetchTask.__init__(self, fetcher, kwargs["url"], kwargs["user_data"])

class FileFetcher(Fetcher):
    def __init__(self):
        Fetcher.__init__(self)

    def supports(self, **kwargs):
        schema = _get_url_schema(kwargs["url"])
        return schema == "file"

    def create_task(self, **kwargs):
        return FileFetchTask(self, **kwargs)

    def fetch(self, task):
        url = task.url
        content = None
        error = None

        try:
            f = open(url[len("file://"):], "r")
            content = f.read()
            f.close()
        except Exception, e:
            error = e

        return FetchResult(content, error)

_file_fetcher = FileFetcher()
_http_fetcher = HttpFetcher()

class FetchJobHandler(ThreadPoolJobHandler):
    job_id = "fetch"

    def __init__(self, id, job):
        ThreadPoolJobHandler.__init__(self, id, job)

        self.pool_size = 5
        self.task_class = FetchTaskThread

    def _prepare(self):
        for task in self.job.tasks:
            self.task_queue.put(task)

class FetchTaskThread(TaskThread):
    def __init__(self, handler):
        TaskThread.__init__(self, handler)

    def _process(self, task):
        return task.fetcher.fetch(task)

JobManager.register_handler(FetchJobHandler)

def create_task(**kwargs):
    for fetcher in [ _http_fetcher, _file_fetcher ]:
        if fetcher.supports(**kwargs):
            return fetcher.create_task(**kwargs)

def fetch(tasks, observers = {}, wait = False):
    job = Job("fetch")
    job.tasks = tasks
    job.observers = observers
    return JobManager.start(job, wait = wait)

def stop(id):
    JobManager.stop(id)
