"""
Provides a module for handling the properties of a Category.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from error import log, logparam, logtb
from gtk.glade import XML
from straw import helpers
from xml.sax import saxutils
import Config
import MVP
import SummaryParser
import gettext
import gobject
import gtk
import os, os.path
import straw.defs
import time

class FeedPropertyView(MVP.GladeView):

    COLUMN_NAME = 0
    COLUMN_MEMBER = 1
    COLUMN_OBJECT = 2

    def _initialize(self):
        self._category = None
        self._defaults = {'title':'','url':'','username':'','password':''}
        self._window = self._widget.get_widget('feed_properties')

        self._title_entry = self._widget.get_widget('title_entry')
        self._location_entry = self._widget.get_widget('location_entry')
        self._website_entry = self._widget.get_widget('website_entry')
        self._description_label = self._widget.get_widget('description_label')
        self._copyright_label = self._widget.get_widget('copyright_label')

        self._username = self._widget.get_widget('properties_username_entry')
        self._password = self._widget.get_widget('properties_password_entry')

        self._next_refresh_label = self._widget.get_widget('properties_next_refresh_label')
        self._previous_refresh_label = self._widget.get_widget('properties_previous_refresh_label')

        self._restore_button = self._widget.get_widget('properties_reset_button')
        self._refresh_spin = self._widget.get_widget('properties_refresh_spin')
        self._articles_spin = self._widget.get_widget('properties_articles_spin')
        self._refresh_default_check = self._widget.get_widget('properties_keep_refresh_default')
        self._articles_default_check = self._widget.get_widget('properties_keep_articles_default')

    def set_feed(self, feed):
        self._category = feed

    def show(self):
        self._initializing_window = True
        self.display_properties()
        self._window.present()
        self._initializing_window = False

    def hide(self, *args):
        self._window.hide()
        self._window.destroy()

    def _on_feed_properties_delete_event(self, *args):
        self.hide()
        return True

    def _on_properties_close_button_clicked(self, *args):
        self.hide()
        return

    def _on_location_button_clicked(self, *args):
        helpers.url_show(self._location_entry.get_text())

    def _on_website_button_clicked(self, *args):
        helpers.url_show(self._website_entry.get_text())

    def display_properties(self):
        self._window.set_title(_("%s Properties") % self._category.name)

        self._title_entry.set_text(self._category.name)

    def _load_uri(self, widget, event):
        helpers.url_show(widget.get_uri())

    def restore_defaults(self):
        # FIXME: add frequency and number of items and the default flags
        self._category.name = self._defaults['title']
        self._category.access_info = (
            self._defaults['url'], self._defaults['username'], self._defaults['password'])
        self._title.set_text(self._category.name)
        self._location.set_text(self._category.location)
        self._username.set_text(self._defaults.get('username',''))
        self._password.set_text(self._defaults.get('password',''))
        self._refresh_default_check.set_active(self._refresh_keep)
        if not self._refresh_keep:
            self._refresh_spin.set_value(float(self._refresh_default))
        self._articles_default_check.set_active(self._articles_keep)
        if not self._articles_keep:
            self._articles_spin.set_value(float(self._articles_default))
        self._restore_button.set_sensitive(False)

    def _on_close_button_clicked(self, *args):
        self.hide()

    def _on_properties_title_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_title_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_title_entry_focus_out_event(self, widget, *args):
        self._category.name = widget.get_text().strip()

    def _on_properties_location_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_location_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_location_entry_focus_out_event(self, widget, *args):
        loc, username, pw = self._category.access_info
        self._category.access_info = (widget.get_text().strip(), username, pw)

    def _on_properties_username_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_username_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(False)

    def _on_properties_username_entry_focus_out_event(self, widget, *args):
        self._presenter.set_username(widget.get_text().strip())

    def _on_properties_password_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_password_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(False)

    def _on_properties_password_entry_focus_out_event(self, widget, *args):
        self._presenter.set_password(widget.get_text().strip())

    def _on_properties_reset_button_clicked(self, *args):
        self.restore_defaults()

    def _on_properties_refresh_spin_focus_out_event(self, widget, *args):
        widget.update()
        value = widget.get_value_as_int()
        self._presenter.set_poll_frequency(value)

    def _on_properties_articles_spin_focus_out_event(self, widget, *args):
        widget.update()
        value = widget.get_value_as_int()
        self._presenter.set_items_stored(value)

    def _on_properties_articles_spin_value_changed(self, widget, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_refresh_spin_value_changed(self, widget, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_keep_refresh_default_toggled(self, widget, *args):
        if self._initializing_window:
            return
        isactive = widget.get_active()
        if isactive:
            self._presenter.set_poll_frequency(feeds.Feed.DEFAULT)
            self._refresh_spin.set_value(float(Config.get_instance().poll_frequency / 60))
        else:
            self._presenter.set_poll_frequency(self._refresh_spin.get_value_as_int() * 60)

        self._refresh_spin.set_sensitive(not isactive)
        self._restore_button.set_sensitive(True)

    def _on_properties_keep_articles_default_toggled(self, widget, *args):
        if self._initializing_window:
            return
        isactive = widget.get_active()
        if isactive:
            self._presenter.set_items_stored(feeds.Feed.DEFAULT)
            self._articles_spin.set_value(float(Config.get_instance().number_of_items_stored))
        else:
            self._presenter.set_items_stored(self._articles_spin.get_value_as_int())
        self._articles_spin.set_sensitive(not isactive)
        self._restore_button.set_sensitive(True)

class FeedPropertyPresenter(MVP.BasicPresenter):

    TIME_INTERVAL = 60

    def _initialize(self):
        self._category = None

    def set_feed(self, feed):
        self._category = feed
        self._view.set_feed(self._category)

    def set_username(self, username):
        loc, uname, pw = self._category.access_info
        self._category.access_info = (loc, username, pw)

    def set_password(self, password):
        loc, uname, pw = self._category.access_info
        self._category.access_info = (loc, uname, password)

    def set_poll_frequency(self, pf):
        if pf != self._category.poll_frequency:
            self._category.poll_frequency = pf * FeedPropertyPresenter.TIME_INTERVAL

    def set_items_stored(self, nitems):
        if nitems != self._category.number_of_items_stored:
            self._category.number_of_items_stored = nitems

def show(feed):
    gladefile = XML(os.path.join(straw.defs.STRAW_DATA_DIR, "node-properties.glade"))
    dialog = FeedPropertyPresenter(view=FeedPropertyView(gladefile))
    dialog.set_feed(feed)

    dialog.view.show()

def read_text(fragment, chars):
    """Read chars cdata characters from html fragment fragment"""
    parser = SummaryParser.TitleImgParser()
    parser.feed(fragment)
    text = parser.get_text(chars)
    parser.close()
    return text
