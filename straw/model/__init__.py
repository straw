from bisect import insort
from gobject import GObject
from straw.error import debug
import gobject
import straw

class AsyncGObject(gobject.GObject):
    def __init__(self):
        gobject.GObject.__init__(self)

    def emit(self, *args):
        gobject.idle_add(gobject.GObject.emit, self, *args)

class Node(gobject.GObject):
    """
    Represents a base of single node in the model tree. This class is meant
    for subclassing only, Node instances should not be used.
    """

    persistent_properties = [ "id", "parent_id", "type", "norder" ]
    persistent_table = "nodes"
    primary_key = "id"
    pdict_table = "node_pdict_entries"

    __gproperties__ = {
        "norder": (int, "parent_id", "norder", 0, 999999, 0, gobject.PARAM_READWRITE),
        "parent": (gobject.TYPE_PYOBJECT, "parent", "parent", gobject.PARAM_READWRITE),
        "unread-count": (int, "unread-count", "unread-count", 0, 999999, 0, gobject.PARAM_READWRITE)
    }

    __gsignals__ = {
        "parent-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "norder-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "unread-count-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT)),
        "mark-all-items-as-read": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }

    def __init__(self):
        GObject.__init__(self)
        self.id = None
        self.parent_id = None
        self.unread_count = 0
        self.norder = 0
        self.children = []
        self.handler_ids_unread_changed = {}
        self.pdict = {}

    def __cmp__(self, other):
        if other == None:
            return -1
        elif self.norder == other.norder:
            return 0
        elif self.norder < other.norder:
            return -1
        else:
            return 1

    def do_get_property(self, property):
        if property.name == "norder":
            return self.norder
        elif property.name == "parent_id":
            return self.parent_id
        elif property.name == "unread-count":
            return self.unread_count

    def do_set_property(self, property, value):
        if property.name == "unread-count":
            if value == None:
                value = 0

            if self.unread_count != value:
                delta = value - self.unread_count
                old_value = self.unread_count
                self.unread_count = value
                self.emit("unread-count-changed", old_value, delta)
        elif property.name == "norder":
            if self.norder != value:
                old_value = self.norder
                self.norder = value
                self.emit("norder-changed", old_value)
        elif property.name == "parent":
            if self.parent.id != value.id:
                old_value = self.parent
                self.parent = value
                self.parent_id = value.id
                self.emit("parent-changed", old_value)

    def is_parent(self):
        raise NotImplementedError

    def add_child(self, node, norder = None, allow_append = True, emit_changes = True):
        debug("adding %d to %d with node.norder = %d, norder = %s" % (node.id, self.id, node.norder, str(norder)))
        if allow_append:
            if norder == None or norder >= len(self.children):
                norder = len(self.children)
        else:
            if node.parent == self:
                if norder > len(self.children):
                    norder = len(self.children)#raise AttributeError

                if norder == None:
                    norder = len(self.children) - 1
            else:
                if norder == None:
                    norder = len(self.children)
                elif norder > len(self.children):
                    norder = len(self.children)

        if norder < len(self.children):
            for child in self.children[norder:]:
                child.props.norder += 1
        
        debug("calculated norder = %d" % (norder))

        node.props.norder = norder

        self.append_child(node, emit_changes = emit_changes)

    def append_child(self, node, emit_changes = True):
        #debug("appending %d to %d with norder = %d" % (node.id, self.id, node.norder))
        insort(self.children, node)
        self.handler_ids_unread_changed[node.id] = node.connect("unread-count-changed", self.on_unread_count_changed)
        
        if emit_changes:
            self.props.unread_count += node.unread_count

    def remove_child(self, node, emit_changes = True):
        debug("removing %d from %d with norder = %d" % (node.id, self.id, node.norder))
        for child in self.children[(node.norder + 1):]:
            child.props.norder -= 1

        self.children.remove(node)
        
        node.disconnect(self.handler_ids_unread_changed[node.id])

        if emit_changes:
            self.props.unread_count -= node.unread_count

    def move(self, new_parent, new_norder = None):
        debug("moving %d to %d (new_norder = %d)" % (self.id, new_parent.id, new_norder))
        old_parent = self.parent
        self.parent.remove_child(self, emit_changes = False)
        new_parent.add_child(self, norder = new_norder, allow_append = False, emit_changes = False)
        self.props.parent = new_parent
        debug("changing unread count (old_parent %d: %d -> %d)" % (old_parent.id, old_parent.unread_count, old_parent.unread_count - self.unread_count))
        debug("changing unread count (new_parent %d: %d -> %d)" % (new_parent.id, new_parent.unread_count, new_parent.unread_count + self.unread_count))
        old_parent.props.unread_count -= self.unread_count
        self.parent.props.unread_count += self.unread_count

    def get_by_path(self, path):
        #print "sss %d | %s | %s" % (self.id, str(path), str(len(self.children)))
        if len(path) == 0:
            return self
        elif path[0] < len(self.children):
            return self.children[path[0]].get_by_path(path[1:])
        else:
            return None
    
    def all_children(self):
        for child_node in self.children:
            yield child_node
            
            for _child_node in child_node.all_children():
                yield _child_node

    def on_unread_count_changed(self, obj, old_value, delta):
        pass

class Category(Node):
    """
    Represents a category - a node that can have child nodes.
    """

    persistent_properties = [ "id", "name" ]
    persistent_table = "categories"
    primary_key = None
    inheritance = True

    def __init__(self):
        GObject.__init__(self)
        Node.__gobject_init__(self)
        Node.__init__(self)

        self.id = None
        self.name = ""
        self.type = "C"

    def is_parent(self):
        return True

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        else:
            return Node.do_get_property(self, property)

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        else:
            Node.do_set_property(self, property, value)

    def on_unread_count_changed(self, obj, old_value, delta):
        self.props.unread_count += delta

    def mark_items_as_read(self):
        for child in self.children:
            child.mark_items_as_read()

class Feed(Node):
    """
    Represents a feed - a node that can contain items. Feeds cannot have
    child nodes.
    """

    persistent_properties = [ "id", "title", "location", "link", "description",
                             "copyright" ]
    persistent_table = "feeds"
    primary_key = None
    inheritance = True

    __gproperties__ = {
        "title":       (str, "title", "title", "", gobject.PARAM_READWRITE), 
        "location":    (str, "location", "location", "", gobject.PARAM_READWRITE), 
        "status": (int, "status", "status", 0, 9999, 0, gobject.PARAM_READWRITE),
    }

    def __init__(self):
        GObject.__init__(self)
        Node.__gobject_init__(self)
        Node.__init__(self)

        self.id = None
        self.type = "F"
        self.items = []
        self.description = ""
        self.copyright = ""

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        elif property.name == "location":
            return self.title
        elif property.name == "status":
            return self.status
        else:
            return Node.do_get_property(self, property)

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        elif property.name == "location":
            self.location = value
        elif property.name == "status":
            self.status = value
        else:
            Node.do_set_property(self, property, value)

    def is_parent(self):
        return False

    def add_child(self, node, norder = None, allow_append = True):
        debug("tried to add child to a feed %d!" % self.id)

    def on_unread_count_changed(self, obj, old_value, delta):
        pass

    def add_item(self, item):
        self.items.append(item)

    def on_is_read_changed(self, obj, is_read):
        if is_read:
            self.props.unread_count -= 1
        else:
            self.props.unread_count += 1

    def mark_items_as_read(self):
        self.emit("mark-all-items-as-read")

class Item(GObject):
    persistent_properties = [ "id", "title", "feed_id", "is_read", "link", "pub_date", "description" ]
    persistent_table = "items"
    primary_key = "id"

    __gproperties__ = {
        "title":       (str, "title", "title", "", gobject.PARAM_READWRITE), 
        "location":    (str, "location", "location", "", gobject.PARAM_READWRITE), 
        "is-read":    (gobject.TYPE_BOOLEAN, "is-read", "is-read", False, gobject.PARAM_READWRITE)
       }
    
    __gsignals__ = {
        "is-read-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
    }

    def __init__(self):
        GObject.__init__(self)
        self.id = None
        self.title = ""
        self.feed_id = None
        self.is_read = False
        
        self.link = ""
        self.pub_date = None
        self.description = ""
        self.source = None
        self.guid = ""
        self.guidislink = False
        
        self.publication_name = ""
        self.publication_volume = ""
        self.publication_number = ""
        self.publication_section = ""
        self.publication_starting_page = ""
        
        self.fm_license = ""
        self.fm_changes = ""
        
        self.creator = ""

        self.contributors = []
        self.enclosures = []
        self.license_urls = []

    def do_get_property(self, property):
        if property.name == "title":
            return self.title
        elif property.name == "is-read":
            return self.is_read
        else:
            raise AttributeError, "unknown property %s" % property.name

    def do_set_property(self, property, value):
        if property.name == "title":
            self.title = value
        elif property.name == "is-read":
            old_value = bool(self.is_read)
            self.is_read = value

            if old_value != value:
                self.emit("is-read-changed", bool(value))
        else:
            raise AttributeError, "unknown property %s" % property.name
