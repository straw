""" preferences.py

Module setting user preferences.
"""
__copyright__ = """
Copyright (c) 2002-2004 Juri Pakaste
Copyright (c) 2005-2007 Straw Contributors
"""
__license__ = """ GNU General Public License

Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Constants import *
from gtk.glade import XML
from os.path import join
import Config
import MVP
import gtk
import os
import straw.defs

class PreferencesView(MVP.GladeView):
    def _initialize(self):
        self._window = self._widget.get_widget('preferences')
        
        self._controls = {}

        self._controls[OPTION_WEB_BROWSER_CMD] = self._widget.get_widget("custom_command_web_browser_entry")
        self._controls[OPTION_POLL_FREQUENCY] = self._widget.get_widget("poll_frequency_spin")
        self._controls[OPTION_ITEMS_STORED] = self._widget.get_widget("number_of_items_spin")
        self._controls[OPTION_PROXY_SERVER] = self._widget.get_widget("proxy_server_entry")
        self._controls[OPTION_PROXY_PORT] = self._widget.get_widget("proxy_port_spin")
        self._controls[OPTION_PROXY_AUTH_ACTIVE] = self._widget.get_widget("proxy_auth_active_checkbox")
        self._controls[OPTION_PROXY_AUTH_USERNAME] = self._widget.get_widget("proxy_auth_username_entry")
        self._controls[OPTION_PROXY_AUTH_PASSWORD] = self._widget.get_widget("proxy_auth_password_entry")

        for option, control in self._controls.iteritems():
            name = control.get_name()

            if name.endswith("_entry"):
                control.set_text(Config.get(option))
            elif name.endswith("_spin"):
                control.set_value(float(Config.get(option)))
            elif name.endswith("_checkbox"):
                control.set_active(Config.get(option))

        self.proxy_type = Config.get(OPTION_PROXY_TYPE)
        radio = self._widget.get_widget("%s_proxy_type_radio" % self.proxy_type)

        if radio:
            radio.set_active(True)

        self.web_browser_type = Config.get(OPTION_WEB_BROWSER_TYPE)
        radio = self._widget.get_widget("%s_web_browser_type_radio" % self.web_browser_type)

        if radio:
            radio.set_active(True)

        self._update_proxy_inputs(Config.get(OPTION_PROXY_TYPE))
        self._update_web_browser_inputs(Config.get(OPTION_WEB_BROWSER_TYPE))

    def _apply(self):
        for option, control in self._controls.iteritems():
            name = control.get_name()

            if name.endswith("_entry"):
                Config.set(option, control.get_text())
            elif name.endswith("_spin"):
                Config.set(option, control.get_value())
            elif name.endswith("_checkbox"):
                Config.set(option, control.get_active())

        Config.set(OPTION_PROXY_TYPE, self.proxy_type)
        Config.set(OPTION_WEB_BROWSER_TYPE, self.web_browser_type)

    def show(self, *args):
        self._window.present()

    def hide(self, *args):
        self._window.hide()
        self._window.destroy()

    def _on_ok_button_clicked(self, button):
        self._apply()
        self.hide()

    def _on_cancel_button_clicked(self, button):
        self.hide()

    def _on_apply_button_clicked(self, button):
        self._apply()

    def on_preferences_delete_event(self, *args):
        self.hide()
        return True

    def on_close_button_clicked(self, button):
        self.hide()

    def _update_proxy_inputs(self, new_type):
        self.proxy_type = new_type
        sensitive = (self.proxy_type == "manual")

        self._controls[OPTION_PROXY_SERVER].set_sensitive(sensitive)
        self._controls[OPTION_PROXY_PORT].set_sensitive(sensitive)
        self._controls[OPTION_PROXY_AUTH_ACTIVE].set_sensitive(sensitive)
        self._controls[OPTION_PROXY_AUTH_USERNAME].set_sensitive(sensitive)
        self._controls[OPTION_PROXY_AUTH_PASSWORD].set_sensitive(sensitive)

    def _on_no_proxy_radio_toggled(self, button):
        self._update_proxy_inputs("none")

    def _on_gnome_proxy_radio_toggled(self, button):
        self._update_proxy_inputs("gnome")

    def _on_manual_proxy_radio_toggled(self, button):
        self._update_proxy_inputs("manual")

    def _update_web_browser_inputs(self, new_type):
        self.web_browser_type = new_type
        sensitive = (self.web_browser_type == "custom")

        self._controls[OPTION_WEB_BROWSER_CMD].set_sensitive(sensitive)

    def _on_gnome_web_browser_radio_toggled(self, button):
        self._update_web_browser_inputs("gnome")

    def _on_custom_command_web_browser_entry_toggled(self, button):
        self._update_web_browser_inputs("custom")

class PreferencesPresenter(MVP.BasicPresenter):
    pass

def show():
    gladefile = XML(os.path.join(straw.defs.STRAW_DATA_DIR, "preferences.glade"))
    dialog = PreferencesPresenter(view = PreferencesView(gladefile))

    dialog.view.show()
