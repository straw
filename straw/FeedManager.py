from Constants import *
from error import debug
from gobject import GObject, SIGNAL_RUN_FIRST
from model import Feed, Item, Category
from storage import DAO, Storage
import FeedUpdater
import ItemManager
import gobject
import opml

_storage_path = None

def init():
    fm = _get_instance()
    fm.init_storage(_storage_path)

def setup(storage_path = None):
    global _storage_path
    _storage_path = storage_path

def import_opml(path, category):
    fm = _get_instance()
    fm.import_opml(path, category)

def export_opml(root_id, filename):
    fm = _get_instance()
    fm.export_opml(root_id, filename)

def lookup_feed(id):
    fm = _get_instance()
    return fm.lookup_feed(id)

def lookup_category(ctg_id):
    fm = _get_instance()
    return fm.lookup_category(ctg_id)

def get_feeds():
    fm = _get_instance()
    return fm.get_feeds()

def update_all_feeds(observers):
    fm = _get_instance()
    return fm.update_all_feeds(observers)

def is_update_all_running():
    fm = _get_instance()
    return fm.update_all_id != None

def stop_update_all():
    fm = _get_instance()
    return fm.stop_update_all()

def update_nodes(nodes, observers = {}):
    fm = _get_instance()
    return fm.update_nodes(nodes, observers)

def save_category(category):
    fm = _get_instance()
    return fm.save_category(category)

def save_feed(feed):
    fm = _get_instance()
    result = fm.save_feed(feed)
    return result

def save_all(save_list):
    fm = _get_instance()
    fm.save_all(save_list)

def get_model():
    fm = _get_instance()
    return fm.nodes

def get_children_feeds(node_id):
    node = lookup_category(node_id)

    if not node:
        return None

    return [_node for _node in node.all_children() if _node.type == "F"]

def categories(root_id = 1):
    nodes = get_model()

    if not nodes.has_key(root_id):
        return

    yield nodes[root_id]

    for node in nodes[root_id].children:
        if node.type == "C":
            for child in categories(node.id):
                yield child

def move_node(node, target_path):
    fm = _get_instance()
    fm.move_node(node, target_path)

def delete_nodes(node_ids):
    fm = _get_instance()
    fm.delete_nodes(node_ids)

class FeedManager(GObject):
    __gsignals__ = {
        "item-added": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "feed-added": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "feed-status-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "category-added": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "update-all-done": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }

    def __init__(self):
        GObject.__init__(self)

        self.storage = None
        self.update_all_id = None

    def emit(self, *args):
        gobject.idle_add(gobject.GObject.emit, self, *args)

    def init_storage(self, path):
        self.storage = Storage(path)
        self.dao = DAO(self.storage)
        self.nodes = self.dao.get_nodes()

        self._prepare_model()

    def _prepare_model(self):
        for node in self.nodes.values():
            self._setup_node_signals(node)

            if node.parent_id != None:
                node.parent = self.nodes[node.parent_id]
                node.parent.append_child(node)
            else:
                node.parent = None

            if isinstance(node, Feed):
                self._set_feed_status(node, FS_IDLE)

    def _setup_node_signals(self, node):
        node.connect("parent-changed", self.on_parent_changed)
        node.connect("norder-changed", self.on_norder_changed)
        node.connect("mark-all-items-as-read", self.on_mark_all_items_as_read)

    def _set_feed_status(self, feed, status):
        if not hasattr(feed, "status"):
            feed.status = None

        if feed.status != status:
            feed.status = status
            self.emit("feed-status-changed", feed)

    def import_opml(self, path, category):
        # XXX: We should support remote locations for OPML import!
        url = "file://" + path
        opml.import_opml(url, category, [ { "opml-imported": [ self._on_opml_imported ] } ])

    def _on_opml_imported(self, handler, save_list):
        if save_list:
            self.dao.tx_begin()
            self.save_all(save_list)
            self.dao.tx_commit()

    def export_opml(self, root_id, filename):
        if not self.nodes.has_key(root_id):
            return None

        opml.export(self.nodes[root_id], filename)

    def _model_add_node(self, node):
        self.nodes[node.parent_id].add_child(node)

    def move_node(self, node, target_path):
        debug("fm:move_node %d to %s" % (node.id, str(target_path)))
        new_parent = self.nodes[1].get_by_path(target_path[:-1])
        
        if not new_parent:
            new_parent = self.nodes[1]

        self.dao.tx_begin()
        node.move(new_parent, target_path[-1:][0])
        self.dao.tx_commit()

    def delete_nodes(self, node_ids):
        self.dao.tx_begin()

        for id in node_ids:
            self.delete_node(id)

        self.dao.tx_commit()

    def delete_node(self, id):
        if not self.nodes.has_key(id):
            return

        node = self.nodes[id]

        children = list(node.children)
        children.reverse()

        for child_node in children:
            self.delete_node(child_node.id)

        node.parent.remove_child(node)
        del self.nodes[id]

        if node.type == "F":
            debug("deleting F %s" % id)
            self.dao.delete_feed(id)
        elif node.type == "C":
            debug("deleting C %s" % id)
            self.dao.delete_category(id)

    def save_all(self, save_list):
        for item in save_list:
            if isinstance(item, Feed):
                self._set_feed_status(item, FS_IDLE)
                item.parent_id = 1

                if item.parent:
                    item.parent_id = item.parent.id
                else:
                    item.parent_id = 1
                    item.parent = self.nodes[1]

                self.save_feed(item)
            else:
                if item.parent:
                    item.parent_id = item.parent.id
                else:
                    item.parent_id = 1
                    item.parent = self.nodes[1]

                self.save_category(item)

    def lookup_feed(self, id):
        return self.nodes[id]

    def lookup_category(self, id):
        return self.nodes[id]

    def save_feed(self, feed):
        if not feed.parent_id:
            feed.parent_id = 1

        category = self.lookup_category(feed.parent_id)

        if not category:
            return None

        if feed.id:
            result = self.dao.save(feed)
        else:
            result = self.dao.save(feed)
            self._model_add_node(feed)
            self.dao.save(feed)

            self.nodes[feed.id] = feed

            self._setup_node_signals(feed)
            self.emit("feed-added", feed)

        return result

    def update_all_feeds(self, observers):
        feeds = [node for node in self.nodes.values() if node.type == "F"]

        self.update_all_id = FeedUpdater.update(feeds, [{
                                         "job-done": [ self._on_update_all_done ],
                                         "update-started": [ self._on_update_feed_start ],
                                         "update-done": [ self._on_update_feed_done ]
                                         }, observers])

    def update_nodes(self, nodes, observers):
        feeds = []

        for node in nodes:
            if node.type == "F":
                feeds.append(node)
            else:
                feeds.extend([child_node for child_node in node.all_children() if child_node.type == "F"])

        FeedUpdater.update(feeds, [{
                                         "update-started": [ self._on_update_feed_start ],
                                         "update-done": [ self._on_update_feed_done ]
                                         }, observers])

    def stop_update_all(self):
        FeedUpdater.stop(self.update_all_id)

    def _on_update_all_done(self, handler, data):
        self.update_all_id = None
        self.emit("update-all-done")

    def _on_update_feed_start(self, handler, feed):
        self._set_feed_status(feed, FS_UPDATING)

    def _on_update_feed_done(self, handler, update_result):
        feed = update_result.feed

        if update_result.error:
            feed.update_result = update_result
            self._set_feed_status(feed, FS_ERROR)
            return

        self._set_feed_status(feed, FS_IDLE)

        self.dao.tx_begin()

        # Some metadata could change between the updates.        
        self.save_feed(feed)

        for item in feed.items:
            item.feed_id = feed.id

            if not ItemManager.feed_item_exists(item):
                self.dao.save(item)
                feed.props.unread_count += 1
                self.emit("item-added", item)

        self.dao.tx_commit()

    def save_category(self, category):
        debug("saving category %s with parent %s" % (category.name, str(category.parent_id)))

        if category.parent and not category.parent_id:
            category.parent_id = category.parent.id

        self.dao.tx_begin()

        self.dao.save(category)
        self._model_add_node(category)
        self.dao.save(category)

        self.dao.tx_commit()

        self.nodes[category.id] = category

        self._setup_node_signals(category)
        self.emit("category-added", category)

    def on_parent_changed(self, obj, old_parent):
        #debug("parent changed, saving %d" % (obj.id))
        self.dao.save(obj)

    def on_norder_changed(self, obj, old_norder):
        #debug("norder changed, saving %d" % (obj.id))
        self.dao.save(obj)

    def on_mark_all_items_as_read(self, node):
        self.dao.tx_begin()
        result = self.dao.query("UPDATE items SET is_read = 1 WHERE feed_id = %d" % node.id)
        self.dao.tx_commit()

        node.props.unread_count -= node.props.unread_count

_instance = None

def _get_instance():
    global _instance
    if not _instance:
        _instance = FeedManager()
    return _instance
