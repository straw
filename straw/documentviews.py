"""
DocumentView is a generic interface to html document browsing.
Methods:
__init__(self, userdir)
widget(self)
load_url(self, url)
stop_load(self)
render_data(self, data, base_uri, mime_type)

Signals:
Required methods:
location_changed(location)
loading_started
loading_finished
status_changed(text)
open_uri(uri)
"""

import sys

import gtk
import gobject

class DocumentView(gobject.GObject):
    __gsignals__ = {
        "location_changed" : (
            gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, [
            gobject.TYPE_STRING]),      # The new location
        "loading_started" : (
            gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, []),
        "loading_finished" : (
            gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, []),
        "status_changed" : (
            gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, [
            gobject.TYPE_STRING]),      # The status
        "open_uri": (
            gobject.SIGNAL_RUN_LAST, gobject.TYPE_BOOLEAN, [
            gobject.TYPE_STRING])       # URI
        }
    def __init__(self, emitOnIdle=False):
        gobject.GObject.__init__(self)
        self.emitOnIdle = emitOnIdle

    def emit(self, *args):
        """
        Override the gobject signal emission so that signals
        can be emitted from the main loop on an idle handler
        """
        if self.emitOnIdle == True:
            gobject.idle_add(gobject.GObject.emit,self,*args)
        else:
            return gobject.GObject.emit(self,*args)

class WebKitDocumentView(DocumentView):
    """wraps the PyWebKitGtk HTML view in the DocumentView interface"""
    def __init__(self, userdir):
        DocumentView.__init__(self)
        global webkit
        import webkit # you'll need PyWebKitGtk

        self._view = webkit.WebView()
        self.location = ""

        sprefix = '_signal_'
        for method in dir(self.__class__):
            if method.startswith(sprefix):
                signal = method[len(sprefix):]
                self._view.connect(signal, getattr(self, method))

        self._scrolled_window = gtk.ScrolledWindow()
        self._scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self._scrolled_window.add(self._view)

        self._scrolled_window.show_all()

    def widget(self):
        """returns the gtk widget of this view"""
        return self._scrolled_window

    def load_url(self, url):
        self._view.open(url)
        self.location = url
        self.emit("location_changed",self.location)

    def stop_load(self):
        self._view.stop_loading()

    def render_data(self, data, base_uri, mime_type):
        ct = mime_type.split("; ")
        if len(ct) > 1:
            charset = ct[1]
        else:
            charset = "utf-8" # default

        self.emit("location_changed",base_uri)
        self._view.load_string(data, ct[0], charset, base_uri)

    def _signal_load_started(self, object, frame):
        self.emit("loading_started")
    def _signal_load_progress_changed(self, object, progress):
        pass
    def _signal_load_finished(self, object, frame):
        self.emit("loading_finished")
    def _signal_title_changed(self, object, title, uri):
        self.location = uri
        self.emit("location_changed",self.location)
    def _signal_status_bar_text_changed(self, object, text):
        pass


class GtkHtmlDocumentView(DocumentView):
    """wraps the GTK HTML view in the DocumentView interface"""
    def __init__(self, userdir):
        DocumentView.__init__(self)
        global gtkhtml2
        import gtkhtml2       # you'll need Debian package python-gnome2-extras

        self._view = gtkhtml2.View()
        self._document = gtkhtml2.Document()
        self._widget = gtk.ScrolledWindow()
        self._widget.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self._widget.add(self._view)
        self.location = ""

        self._view.connect("on_url", self._signal_on_url)
        self._document.connect("link_clicked", self._signal_link_clicked)
        self._document.connect("request-url", self._signal_request_url)

        self._view.set_document(self._document)

    def widget(self):
        """returns the gtk widget of this view"""
        return self._widget

    def load_url(self, url):
        res = self._open_url(self._complete_url(url))
        ct = res.info()['content-type']
        self.render_data(res.read(), res.geturl(), ct)

    def render_data(self, data, base_uri, mime_type):
        self.location = base_uri
        self.emit("location_changed", self.location)

        ct = mime_type.split("; ")
        if len(ct) > 1:
            charset = ct[1]
        else:
            charset = "utf-8" # default

        self._document.clear()
        print "clear"
        self._document.open_stream(ct[0])
        print "open"
        self._document.write_stream(data)
        print "write"
        self._document.close_stream()
        print "close"
        return

    def _signal_on_url(self, object, url):
        if url == None: url = ""
        else: url = self._complete_url(url)
        self.emit("status_changed", url)

    def _signal_link_clicked(self, object, link):
        self.emit("open_uri", self._complete_url(link))

    def _signal_request_url(self, object, url, stream):
        stream.write(self._fetch_url(self._complete_url(url)))

    def _open_url(self, url, headers=[]):
        import urllib2
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', 'Wikitin')]+headers
        return opener.open(url)

    def _fetch_url(self, url, headers=[]):
        return self._open_url(url, headers).read()

    def _complete_url(self, url):
        import string, urlparse, urllib
        url = urllib.quote(url, safe=string.punctuation)
        if urlparse.urlparse(url)[0] == '':
            return urlparse.urljoin(self.location, url)
        else:
            return url

    
class MozEmbedDocumentView(DocumentView):
    """wraps the GTK embeddable Mozilla in the DocumentView interface"""
    def __init__(self, profiledir):
        DocumentView.__init__(self)
        global gtkmozembed
        import gtkmozembed    # you'll need Debian package python-gnome2-extras

        print "Setting Mozilla profile dir to %s name %s" % (profiledir, 'default')
        gtkmozembed.set_profile_path(profiledir, 'default')

        self.moz = gtkmozembed.MozEmbed()
        self.url_load_request = False # flag to break load_url recursion
        self.location = ""

        
        sprefix = '_signal_'
        for method in dir(self.__class__):
            if method.startswith(sprefix):
                self.moz.connect(method[len(sprefix):], getattr(self, method))

    def widget(self):
        """returns the gtk widget of this view"""
        return self.moz

    def load_url(self, str):
        self.url_load_request = True  # don't handle open-uri signal
        self.moz.load_url(str)        # emits open-uri signal
        self.url_load_request = False # handle open-uri again

    def stop_load(self):
        self.moz.stop_load()
    def go_back(self):
        self.url_load_request = True  # don't handle open-uri signal
        self.moz.go_back()
        self.url_load_request = False # handle open-uri again
    def go_forward(self):
        self.url_load_request = True  # don't handle open-uri signal
        self.moz.go_forward()
        self.url_load_request = False # handle open-uri again
    def reload(self):
        self.moz.reload(gtkmozembed.FLAG_RELOADNORMAL)

    def render_data(self, data, base_uri, mime_type):
        self.url_load_request = True  # don't handle open-uri signal

        ct = mime_type.split("; ")
        if len(ct) > 1:
            charset = ct[1]
        else:
            charset = "utf-8" # default

        self.location_changed(base_uri)

        # gtkmozembed hangs if it's fed more than 2^16 at a time
        # XXX bytes, chars?
        self.moz.open_stream(base_uri, ct[0])
        while True:
            block, data = data[:2**16], data[2**16:]
            self.moz.append_data(block, long(len(block)))
            if len(data) == 0: break
        self.moz.close_stream()

        self.url_load_request = False # handle open-uri again

    def _signal_link_message(self, object):
        self.emit("status_changed", self.moz.get_link_message())

    def _signal_open_uri(self, object, uri):
        if self.url_load_request: return False # proceed as requested
        # otherwise, this is from the page
        # print uri
        import gobject
        if uri.__class__ == gobject.GPointer:
            print "The gpointer bug, guessing...",
            uri = self.moz.get_link_message()
            if uri=="": print "<empty>",
            print uri
            if uri=="":
                return False # XXX can't handle, let MozEmbed do it
            return self.emit("open_uri", uri)
        else:
            print "No gpointer bug here !-)"
            return self.emit("open_uri", uri)
        
    def _signal_location(self, object):
        self.location_changed(self.moz.get_location())

    def location_changed(self, location):
        print "moz: location: "+location
        self.location = location
        self.emit("location_changed",self.location)

    def _signal_progress(self, object, cur, maxim):
        if maxim < 1:
            print "Progress: %d" % cur
        else:
            print 'Progress: %d%%' % (cur/maxim)
    def _signal_net_state(self, object, flags, status):
        print 'net-state flags=%x status=%x' % (flags,status)
    def _signal_new_window(self, object, *args):
        print 'new-window', args
    def _signal_net_start(self, object):
        self.emit("loading_started")
    def _signal_net_stop(self, object):
        self.emit("loading_finished")
        

def get_subdir(dir, subdir=''):
    import os
    userdir = os.path.join(dir, subdir)
    if not os.access(userdir, os.F_OK):
        os.makedirs(userdir)
    return userdir

documentviews = ['webkit', 'gtkmozembed', 'gtkhtml']
create_documentviews = {
    'webkit': lambda userdir:WebKitDocumentView(userdir),
    'gtkmozembed': lambda userdir:MozEmbedDocumentView(get_subdir(userdir, 'mozilla')),
    'gtkhtml': lambda userdir:GtkHtmlDocumentView(userdir),
}

def default_documentview(userdir):
    for documentview in documentviews:
        try:
            return create_documentviews[documentview](userdir)
        except ImportError:
            pass
    else:
        print "No HTML view available!"
        sys.exit(10)

