""" FeedListView.py

Module for displaying the feeds in the Feeds TreeView.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Constants import *
from error import debug
from model import Feed, Item, Category
import Config
import FeedManager
import MVP
import categoryproperties
import feedproperties
import gobject
import gtk
import helpers
import os, copy, locale, logging

class Column:
    pixbuf, name, foreground, unread, object, editable = range(6)

_tmp_widget = gtk.Label()

class TreeViewNode(object):
    def __init__(self, node, store):
        self.node = node
        self.store = store

        self.node.connect("notify", self._on_unread_count_changed)

    def _on_unread_count_changed(self, node, delta):
        self.refresh()

    def refresh(self):
        iter = self.iter

        if not iter:
            return

        self.store.set(iter, Column.pixbuf, self.pixbuf)
        self.store.set(iter, Column.unread, self.node.unread_count)
        self.store.set(iter, Column.name, self.title)

    @property
    def title(self):
        ''' The title of the node be it a category or a feed '''

        if self.node.type == "C":
            title = self.node.name
        elif self.node.type == "F":
            title = self.node.title

        title = helpers.pango_escape(title)

        if hasattr(self.node, "status") and (self.node.status & FS_UPDATING):
            if title:
                title = "<i>" + title + "</i>"

        return title

    @property
    def unread_count(self):
        ''' The title of the node be it a category or a feed '''
        return self.node.unread_count

    @property
    def pixbuf(self):
        ''' gets the pixbuf to display according to the status of the feed '''
        global _tmp_widget

        if isinstance(self.node, Feed):
            if self.node.status == FS_ERROR:
                return _tmp_widget.render_icon(gtk.STOCK_CANCEL, gtk.ICON_SIZE_MENU)
            else:
                return _tmp_widget.render_icon(gtk.STOCK_FILE, gtk.ICON_SIZE_MENU)
        else:
            return _tmp_widget.render_icon(gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_MENU)

    @property
    def path_list(self):
        path = []
        node = copy.copy(self.node)

        while node:
            path.append(node.norder)
            node = copy.copy(node.parent)

        path.pop() # We don't need path to "root" category here since it's not in the tree view.
        path.reverse()

        return path

    @property

    def path(self):
        path = self.path_list

        if len(path) == 0:
            return None
        else:
            return ":".join(map(str, path))

    @property
    def iter(self):
        path = self.path

        if path == None:
            return None
        else:
            try:
                return self.store.get_iter_from_string(path)
            except:
                return None

    @property

    def parent_path(self):
        path = self.path_list
        path.pop()

        if len(path) == 0:
            return None
        else:
            return ":".join(map(str, path))

    @property
    def parent_iter(self):
        path = self.parent_path

        if path == None:
            return None
        else:
            return self.store.get_iter_from_string(path)

class FeedListModel:
    ''' The model for the feed list view '''

    def __init__(self):
        self.refresh_tree()
        self._init_signals()

    def refresh_tree(self):
        self.appmodel = FeedManager.get_model()

        self._prepare_store()
        self._prepare_model()

        self._populate_tree(1, None, [])

    def _init_signals(self):
        FeedManager._get_instance().connect("feed-added", self._on_node_added)
        FeedManager._get_instance().connect("feed-status-changed", self._on_feed_status_changed)
        FeedManager._get_instance().connect("category-added", self._on_node_added)

    def _prepare_model(self):
        self.tv_nodes = dict([(node.id, TreeViewNode(node, self.store)) for node in self.appmodel.values()])

    def _prepare_store(self):
        self.store = gtk.TreeStore(gtk.gdk.Pixbuf, str, str, str, gobject.TYPE_PYOBJECT, bool)

    def _populate_tree(self, parent_id, parent_iter, done):
        for node in self.tv_nodes[parent_id].node.children:
            tv_node = self.tv_nodes[node.id]

            if node.type == "F":
                self._create_row(tv_node)
                tv_node.store = self.store
            elif node.type == "C":
                current_parent = self._create_row(tv_node)
                tv_node.store = self.store

                if self.tv_nodes.has_key(node.id):
                    self._populate_tree(node.id, current_parent, done)

    def _create_row(self, node, editable = False):
        return self.store.append(node.parent_iter, [node.pixbuf,
                                              node.title,
                                              'black',
                                              node.unread_count,
                                              node, editable])

    def _on_node_added(self, src, node):
        tv_node = TreeViewNode(node, self.store)
        self.add_node(tv_node)
        self._create_row(tv_node)

    def _on_feed_status_changed(self, src, feed):
        self.tv_nodes[feed.id].refresh()

    def add_node(self, tv_node):
        self.tv_nodes[tv_node.node.id] = tv_node

    @property
    def model(self):
        return self.store

    def search(self, rows, func, data):
        if not rows: return None
        for row in rows:
            if func(row, data):
                return row
            result = self.search(row.iterchildren(), func, data)
            if result: return result
        return None

class FeedsView(MVP.WidgetView):
    def _initialize(self):
        self._widget.set_search_column(Column.name)

        # pixbuf column
        column = gtk.TreeViewColumn()
        unread_renderer = gtk.CellRendererText()
        column.pack_start(unread_renderer, False)
        column.set_attributes(unread_renderer, text = Column.unread)

        status_renderer = gtk.CellRendererPixbuf()
        column.pack_start(status_renderer, False)
        column.set_attributes(status_renderer, pixbuf = Column.pixbuf)

        # feed title renderer
        title_renderer = gtk.CellRendererText()

        title_renderer.connect("edited", self.on_node_edit_title_edited)
        title_renderer.connect("editing-canceled", self.on_node_edit_title_canceled)

        #title_renderer.set_property('editable', True)
        column.pack_start(title_renderer, False)
        column.set_attributes(title_renderer,
                              foreground=Column.foreground,
                              markup=Column.name,
                              editable=Column.editable) #, weight=Column.BOLD)

        self._widget.append_column(column)

        selection = self._widget.get_selection()
        selection.set_mode(gtk.SELECTION_MULTIPLE)

        self._widget.connect("button_press_event", self._on_button_press_event)
        self._widget.connect("popup-menu", self._on_popup_menu)

        uifactory = helpers.UIFactory('FeedListActions')
        action = uifactory.get_action('/feedlist_popup/refresh')
        action.connect('activate', self.on_menu_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/add_child')
        action.connect('activate', self.on_menu_add_child_activate)
        self.mark_all_as_read_action = uifactory.get_action('/feedlist_popup/mark_as_read')
        self.mark_all_as_read_action.connect('activate', self.on_menu_mark_all_as_read_activate)
        action = uifactory.get_action('/feedlist_popup/stop_refresh')
        action.connect('activate', self.on_menu_stop_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/remove')
        action.connect('activate', self.on_remove_selected_feed)
        action = uifactory.get_action('/feedlist_popup/properties')
        action.connect('activate', self.on_display_properties_feed)
        self.popup = uifactory.get_popup('/feedlist_popup')

        treeview = self._widget

        treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK, [("drop_yes", 0, 0)], gtk.gdk.ACTION_MOVE)
        treeview.enable_model_drag_dest([("drop_yes", 0, 0)], gtk.gdk.ACTION_MOVE)
        treeview.connect("drag_data_received", self._on_dragdata_received)
        treeview.connect("drag_motion", self._on_drag_motion)

    def _on_drag_motion(self, treeview, drag_context, x, y, eventtime):
        temp = treeview.get_dest_row_at_pos(x, y)

        if not temp:
            return

        model = treeview.get_model()
        drop_path, drop_position = temp

        # FIXME: Here we use only first selected node of possibly multiple
        # selected. See fixme comment in self._on_dragdata_received.
        source_node = [tv_node for tv_node in self.selected()][0]

        drop_node = model[drop_path][Column.object]
        source_path = source_node.path_list

        sane_drop_path = self._check_drop_path(model, source_path, drop_path)

        is_drop_into = drop_position == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE or \
            drop_position == gtk.TREE_VIEW_DROP_INTO_OR_AFTER

        can_drop_into = drop_node.node.is_parent()

        if sane_drop_path and ((is_drop_into and can_drop_into) or not is_drop_into):
            treeview.enable_model_drag_dest([("drop_yes", 0, 0)], gtk.gdk.ACTION_MOVE)
        else:
            treeview.enable_model_drag_dest([("drop_no", 0, 0)], gtk.gdk.ACTION_MOVE)

    def _on_dragdata_received(self, treeview, drag_context, x, y, selection, info, eventtime):
        model, pathlist = treeview.get_selection().get_selected_rows()

        if len(pathlist) > 1:
            # FIXME: Maybe we want to support drag and drop for multiple rows?
            # For now it's not worth it while there are other things to do.
            drag_context.finish(False, False, eventtime)
            return
        
        source_path = pathlist[0]
        source_iter = model.get_iter(source_path)

        temp = treeview.get_dest_row_at_pos(x, y)

        if temp != None:
            drop_path, drop_pos = temp
        else:
            drop_path, drop_pos = (len(model) - 1,), gtk.TREE_VIEW_DROP_AFTER

        effective_drop_path = self._calculate_effective_drop_path(source_path, drop_path, drop_pos)

        if source_path == effective_drop_path:
            drag_context.finish(False, False, eventtime)
            return

        drop_iter = model.get_iter(drop_path)

        if not self._check_drop_path(model, source_path, drop_path):
            drag_context.finish(False, False, eventtime)
            return

        node = model[source_path][Column.object].node
        self._iter_copy(model, source_iter, drop_iter, drop_pos)

        drag_context.finish(True, True, eventtime)
        FeedManager.move_node(node, effective_drop_path)

    def _check_drop_path(self, model, source_path, drop_path):
        """
        Verifies if a drop path is not within the subtree of source path so that
        we can disallow dropping parent into its own subtree etc. using this check.
        """

        return list(drop_path[0:len(source_path)]) != list(source_path) 

    def _iter_copy(self, model, iter_to_copy, target_iter, pos):
        """
        Recursively copies GTK TreeView iters from source to target using
        GTK relative data about drag and drop operation.
        """

        path = model.get_path(iter_to_copy)

        if (pos == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE) or (pos == gtk.TREE_VIEW_DROP_INTO_OR_AFTER):
            new_iter = model.prepend(target_iter, model[path])
        elif pos == gtk.TREE_VIEW_DROP_BEFORE:
            new_iter = model.insert_before(None, target_iter, model[path])
        elif pos == gtk.TREE_VIEW_DROP_AFTER:
            new_iter = model.insert_after(None, target_iter, model[path])

        n = model.iter_n_children(iter_to_copy)

        for i in range(n): 
            next_iter_to_copy = model.iter_nth_child(iter_to_copy, n - i - 1)
            self._iter_copy(model, next_iter_to_copy, new_iter, gtk.TREE_VIEW_DROP_INTO_OR_BEFORE)

    def _calculate_effective_drop_path(self, source_path, drop_path, drop_pos):
        """
        Calculate effective absolute drop path given drop_pos and source/destination
        of drag and drop operation. GTK uses relative terms for describing drop
        destination (after/before/into etc.) while we prefer absolute drop path
        and we can take care of reordering ourselves.
        """

        result = list(drop_path)
        same_level = len(source_path) == len(drop_path) and source_path[:-1] == drop_path[:-1]

        if drop_pos == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE:
            result.append(0)
        elif drop_pos == gtk.TREE_VIEW_DROP_INTO_OR_AFTER:
            result.append(0)
        elif drop_pos == gtk.TREE_VIEW_DROP_BEFORE:
            if not same_level or (same_level and source_path[-1] < drop_path[-1]):
                if result[-1] > 0:
                    result[-1] -= 1
        elif drop_pos == gtk.TREE_VIEW_DROP_AFTER:
            if not same_level or (same_level and source_path[-1] > drop_path[-1]):
                result[-1] += 1

        return tuple(result)

    def _model_set(self):
        self._widget.set_model(self._model.model)

    def add_selection_changed_listener(self, listener):
        selection = self._widget.get_selection()
        selection.connect('changed', listener.feedlist_selection_changed, Column.object)

    def add_mark_all_as_read_listener(self, listener):
        self.mark_all_as_read_action.connect('activate', listener.on_mark_all_as_read)

    def _on_popup_menu(self, treeview, *args):
        self.popup.popup(None, None, None, 0, 0)

    def _on_button_press_event(self, treeview, event):
        retval = 0

        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = gtk.get_current_event_time()
            path = treeview.get_path_at_pos(x, y)

            if path is None:
                return 1

            path = path[0]
            self.node_at_popup = self.model.store[path][Column.object]
            treeview.grab_focus()

            if self.selected_count() < 2:
                selection = treeview.get_selection()
                selection.unselect_all()
                selection.select_path(path)

            self.popup.popup(None, None, None, event.button, time)
            retval = 1

        return retval

    def get_selected_node(self):
        nodes = [node for node in self.selected()]

        if len(nodes) > 0:
            return nodes[0].node
        else:
            return None

    def get_expanded_nodes(self):
        expanded = []

        def add(treeview, path, expanded):
            node = treeview.get_model()[path][Column.object].node
            expanded.append(node)

        self._widget.map_expanded_rows(add, expanded)

        return expanded

    def expand_nodes(self, nodes):
        for node_id in nodes:
            if node_id in self.model.tv_nodes:
                path = self.model.tv_nodes[node_id].path
                self._widget.expand_row(path, False)

    def select_node(self, id):
        if not id in self.model.tv_nodes:
            return

        path = self.model.tv_nodes[id].path

        if not path:
            return

        selection = self._widget.get_selection()
        selection.unselect_all()
        self._widget.expand_to_path(path)
        selection.select_path(path)
        self._widget.grab_focus()

    def selected_count(self):
        selection = self._widget.get_selection()
        pathlist = selection.get_selected_rows()[1]
        return len(pathlist)

    def selected(self):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        nodes = [model[path][Column.object] for path in pathlist]

        for tv_node in nodes:
            yield tv_node

    def foreach_selected(self, func):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            for treeiter in iters:
                object = model.get_value(treeiter, Column.object)
                func(object, model, treeiter)
        except TypeError, te:
            logging.exception(te)

    def on_menu_add_child_activate(self, *args):
        node = self.node_at_popup.node

        if not self.node_at_popup.node.is_parent():
            node = node.parent

        self.begin_add_category(node)

    def begin_add_category(self, node):
        category = Category()
        category.parent = node
        category.norder = len(node.children)
        self.new_child = TreeViewNode(category, self.model.store)
        iter = self.model._create_row(self.new_child, editable = True)
        path = self.model.store.get_path(iter)
        column = self._widget.get_column(0)

        parent_path = self.new_child.parent_path

        if parent_path:
            self._widget.expand_row(parent_path, False)

        self._widget.set_cursor_on_cell(path, focus_column = column, start_editing = True)

    def on_menu_poll_selected_activate(self, *args):
        """config = Config.get_instance()

        if config.offline: #XXX
            config.offline = not config.offline"""

        FeedManager.update_nodes([node.node for node in self.selected()])

    def on_menu_stop_poll_selected_activate(self, *args):
        self.foreach_selected(lambda o,*args: o.feed.router.stop_polling())

    def on_menu_mark_all_as_read_activate(self, *args):
        self.foreach_selected(lambda o, *args: o.node.mark_items_as_read())

    def on_remove_selected_feed(self, *args):
        nodes = [tv_node for tv_node in self.selected()]

        FeedManager.delete_nodes([tv_node.node.id for tv_node in nodes])

        for node in nodes:
            iter = node.iter

            if iter:
                self.model.store.remove(iter)

    def on_node_edit_title_canceled(self, cellrenderer):
        if self.new_child:
            debug("on_node_edit_title_canceled (new_child), removing %s" % str(self.new_child.path))
            self.model.store.remove(self.new_child.iter)
            self.new_child = None

    def on_node_edit_title_edited(self, cellrenderertext, path, new_text):
        if len(new_text) > 0:
            self.new_child.node.name = new_text
            FeedManager.save_category(self.new_child.node)

        self.model.store.remove(self.new_child.iter)
        self.new_child = None

    def on_display_properties_feed(self, *args):
        selected_tv_node = [tv_node for tv_node in self.selected()][0]
        self._presenter.show_feed_information(selected_tv_node.node)

    def add_category(self):
        self.begin_add_category(self._model.tv_nodes[1].node)

    def select_first_feed(self):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        treeiter = model.get_iter_first()
        if not treeiter or not model.iter_is_valid(treeiter):
            return False
        self.set_cursor(treeiter)
        return True

    def select_next_feed(self, with_unread=False):
        ''' Scrolls to the next feed in the feed list

        If there is no selection, selects the first feed. If multiple feeds
        are selected, selects the feed after the last selected feed.

        If unread is True, selects the next unread with unread items.

        If the selection next-to-be is a category, go to the iter its first
        child. If current selection is a child, then go to (parent + 1),
        provided that (parent + 1) is not a category.
        '''
        has_unread = False
        def next(model, current):
            treeiter = model.iter_next(current)
            if not treeiter: return False
            if model.iter_depth(current): next(model, model.iter_parent(current))
            path = model.get_path(treeiter)
            if with_unread and model[path][Column.unread] < 1:
                next(model, current)
            self.set_cursor(treeiter)
            return True
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current = iters.pop()
            if model.iter_has_child(current):
                iterchild = model.iter_children(current)
                # make the row visible
                path = model.get_path(iterchild)
                for i in range(len(path)):
                    self._widget.expand_row(path[:i+1], False)
                # select his first born child
                if with_unread and model[path][Column.unread] > 0:
                    self.set_cursor(iterchild)
                    has_unread = True
                else:
                    has_unread = next(model, current)
            has_unread = next(model,current)
        except IndexError:
            self.set_cursor(model.get_iter_first())
            has_unread = True
        return has_unread

    def select_previous_feed(self):
        ''' Scrolls to the previous feed in the feed list.

        If there is no selection, selects the first feed. If there's multiple
        selection, selects the feed before the first selected feed.

        If the previous selection is a category, select the last node in that
        category. If the current selection is a child, then go to (parent -
        1). If parent is the first feed, wrap and select the last feed or
        category in the list.
        '''
        def previous(model, current):
            path = model.get_path(current)
            treerow = model[path[-1]-1]
            self.set_cursor(treerow.iter)
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current_first = iters.pop(0)
            if model.iter_has_child(current_first):
                children = model.iter_n_children(current_first)
                treeiter = model.iter_nth_child(children - 1)
                self.set_cursor(treeiter)
                return
            previous(model, current_first)
        except IndexError:
            self.set_cursor(model.get_iter_first())
        return

    def set_cursor(self, treeiter, col_id=None, edit=False):
        if not treeiter:
            return

        column = None
        path = self._model.model.get_path(treeiter)

        if col_id:
            column = self._widget.get_column(col_id)

        self._widget.set_cursor(path, column, edit)
        self._widget.scroll_to_cell(path, column)
        self._widget.grab_focus()

class FeedsPresenter(MVP.BasicPresenter):
    def _initialize(self):
        self.model = FeedListModel()

    def store_state(self):
        node = self.view.get_selected_node()
        id = -1

        if node:
            id = node.id

        Config.set(OPTION_LAST_SELECTED_NODE, id)
        Config.set(OPTION_LAST_EXPANDED_NODES, ",".join([str(node.id) for node in self.view.get_expanded_nodes()]))

    def restore_state(self):
        id = Config.get(OPTION_LAST_SELECTED_NODE)

        if id != -1:
            self.view.select_node(id)

        expanded = Config.get(OPTION_LAST_EXPANDED_NODES)

        if expanded:
            try:
                expanded = map(int, expanded.split(","))
            except ValueError:
                expanded = []

        self.view.expand_nodes(expanded)

    def add_category(self):
        self.view.add_category()

    def select_first_feed(self):
        return self.view.select_first_feed()

    def select_next_feed(self, with_unread=False):
        return self.view.select_next_feed(with_unread)

    def select_previous_feed(self):
        return self.view.select_previous_feed()

    def show_feed_information(self, node):
        if node.type == "C":
            properties = categoryproperties
        elif node.type == "F":
            properties = feedproperties

        properties.show(node)
