""" FEED STATUS CONSTANTS """

FS_OK          = 1
FS_IDLE        = 2
FS_UPDATING    = 4
FS_ERROR       = 8

""" CONFIG OPTIONS """

OPTION_LAST_POLL = "/general/last_poll"
OPTION_ITEMS_STORED = "/general/number_of_items_stored"
OPTION_ITEM_ORDER = "/general/item_order_newest"
OPTION_WEB_BROWSER_CMD = "/general/browser_cmd"
OPTION_WEB_BROWSER_TYPE = "/general/browser_type"

OPTION_WINDOW_LEFT = "/ui/window_left"
OPTION_WINDOW_TOP = "/ui/window_top"
OPTION_WINDOW_SIZE_H = "/ui/window_height"
OPTION_WINDOW_SIZE_W = "/ui/window_width"
OPTION_WINDOW_MAX = "/ui/window_maximized"

OPTION_MAIN_PANE_POS = "/ui/main_pane_position"
OPTION_SUB_PANE_POS = "/ui/sub_pane_position"

OPTION_MAGNIFICATION = "/ui/text_magnification"
OPTION_PANE_LAYOUT = "/ui/pane_layout"
OPTION_OFFLINE = "/general/offline"
OPTION_POLL_FREQUENCY = "/general/poll_frequency"

OPTION_LAST_SELECTED_NODE = "/ui/last_selected_node"
OPTION_LAST_EXPANDED_NODES = "/ui/last_expanded_nodes"

OPTION_PROXY_TYPE = "/network/proxy_type"
OPTION_PROXY_SERVER = "/network/proxy_server"
OPTION_PROXY_PORT = "/network/proxy_port"
OPTION_PROXY_AUTH_ACTIVE = "/network/proxy_auth_active"
OPTION_PROXY_AUTH_USERNAME = "/network/proxy_auth_username"
OPTION_PROXY_AUTH_PASSWORD = "/network/proxy_auth_password"

config_options_defaults = \
{
    OPTION_LAST_POLL:         0,
    OPTION_ITEMS_STORED:      30,
    OPTION_ITEM_ORDER:        True,
    OPTION_WEB_BROWSER_CMD:   "",
    OPTION_WEB_BROWSER_TYPE:  "gnome",

    OPTION_WINDOW_LEFT:       0,
    OPTION_WINDOW_TOP:        0,
    OPTION_WINDOW_SIZE_W:     640,
    OPTION_WINDOW_SIZE_H:     480,
    OPTION_WINDOW_MAX:        False,

    OPTION_MAIN_PANE_POS:     100,
    OPTION_SUB_PANE_POS:      100,

    OPTION_MAGNIFICATION:     1.0,
    OPTION_PANE_LAYOUT:       "vertical",
    OPTION_OFFLINE:           True,
    OPTION_POLL_FREQUENCY:    1800,

    OPTION_LAST_SELECTED_NODE: -1,
    OPTION_LAST_EXPANDED_NODES: "",

    OPTION_PROXY_TYPE:        "none",
    OPTION_PROXY_SERVER:      "http://example.server/",
    OPTION_PROXY_PORT:        8080,
    OPTION_PROXY_AUTH_ACTIVE:   False,
    OPTION_PROXY_AUTH_USERNAME: "",
    OPTION_PROXY_AUTH_PASSWORD: ""
}

""" NETWORK CONSTANTS """

PRIORITY_IMAGE = 1
PRIORITY_RSS = 2
PRIORITY_DEFAULT = 2^32

MAX_CONNECTIONS = 40
POLL_INTERVAL = 10
POLL_TIMEOUT = 0.05

MAX_DOWNLOAD_SIZE = 2**20
MAX_DOWNLOAD_TIME = 1000
