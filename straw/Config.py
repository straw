""" Config.py

Module for Straw's configuration settings.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """ GNU General Public License

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from Constants import *
from error import log, logtb, logparam
import gconf
import gobject
import os
import pygtk
import urllib
pygtk.require('2.0')

from straw import *

GCONF_STRAW_ROOT = "/apps/straw"

# straw's home directory
def straw_home():
    home = os.getenv('HOME')
    if not home:
        home = os.path.expanduser('~')
    return os.path.join(home, '.straw')

def ensure_directory(strawdir):
    if os.path.exists(strawdir):
        if not os.path.isdir(strawdir):
            raise Exception
        return 0
    os.mkdir(strawdir)
    return 1

class ConfigGConfPersistence:
    SAVERS = { int: "int",
               bool: "bool",
               str: "string",
               float: "float" }

    CASTS = { gconf.VALUE_BOOL:   gconf.Value.get_bool,
              gconf.VALUE_INT:    gconf.Value.get_int,
              gconf.VALUE_FLOAT:  gconf.Value.get_float,
              gconf.VALUE_STRING: gconf.Value.get_string }

    def __init__(self, client):
        self.client = client

    def save_option(self, option, value):
        getattr(self.client, 'set_' + self.SAVERS[type(value)])(
            GCONF_STRAW_ROOT + option, value)

    def load_option(self, key, option_type):
        gval = self.client.get(GCONF_STRAW_ROOT + key)

        if gval == None:
            return None

        value = None

        try:
            value = getattr(gconf.Value, 'get_' + self.SAVERS[option_type])(gval)
        except TypeError, te:
            print "FIXME! TypeError logging (option = %s): %s" % (key, str(te))

        return value

class ConfigPersistence:
    def __init__(self, *backends):
        self.backends = backends

    def save_option(self, option, value):
        for b in self.backends:
            if option in b[1]:
                b[0].save_option(option, value)

    def load_option(self, key, option_type):
        for b in self.backends:
            if key in b[1]:
                return b[0].load_option(key, option_type)

class ProxyConfig(object):
    def __init__(self):
        self.active = False
        self.host = None
        self.port = None
        self.auth = None
        self.username = None
        self.password = None

class GconfProxyConfig(ProxyConfig):
    """Encapsulate proxy use and location information (host, port, ip),
    gconf reading and name lookup logic"""

    GCONF_HTTP_PROXY = "/system/http_proxy"
    GCONF_HTTP_PROXY_USE = GCONF_HTTP_PROXY + "/use_http_proxy"
    GCONF_HTTP_PROXY_HOST = GCONF_HTTP_PROXY + "/host"
    GCONF_HTTP_PROXY_PORT = GCONF_HTTP_PROXY + "/port"
    GCONF_HTTP_PROXY_AUTHENTICATION = GCONF_HTTP_PROXY + "/use_authentication"
    GCONF_HTTP_PROXY_USER = GCONF_HTTP_PROXY + "/authentication_user"
    GCONF_HTTP_PROXY_PASSWORD = GCONF_HTTP_PROXY + "/authentication_password"

    def __init__(self):
        ProxyConfig.__init__(self)
        client = gconf.client_get_default()
        self.active = client.dir_exists(self.GCONF_HTTP_PROXY)

        if not self.active:
            return

        client.add_dir(self.GCONF_HTTP_PROXY, gconf.CLIENT_PRELOAD_RECURSIVE)
        client.notify_add(self.GCONF_HTTP_PROXY_USE, self.proxy_mode_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_HOST, self.proxy_host_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_PORT, self.proxy_port_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_AUTHENTICATION, self.proxy_auth_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_USER, self.proxy_auth_username_changed)
        client.notify_add(self.GCONF_HTTP_PROXY_PASSWORD, self.proxy_auth_password_changed)

        self.active = client.get_bool(self.GCONF_HTTP_PROXY_USE)
        self.host = client.get_string(self.GCONF_HTTP_PROXY_HOST)
        self.port = client.get_int(self.GCONF_HTTP_PROXY_PORT)
        self.auth = client.get_bool(self.GCONF_HTTP_PROXY_AUTHENTICATION)
        if self.auth:
            self.username = client.get_string(self.GCONF_HTTP_PROXY_USER)
            self.password = client.get_string(self.GCONF_HTTP_PROXY_PASSWORD)

    def proxy_mode_changed(self, client, notify_id, entry, *args):
        self.active  = entry.value.get_bool()

    def proxy_host_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        if value:
            self.host = value
        else:
            self.active = False

    def proxy_port_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_int()
        if value:
            self.port = value
        else:
            self.active = False

    def proxy_auth_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_bool()
        if value:
            self.auth = value

    def proxy_auth_username_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        self.username = value

    def proxy_auth_password_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        self.password = value

class EnvironmentProxyConfig(ProxyConfig):
    """Encapsulate proxy use and location information, environment reading"""

    def __init__(self):
        ProxyConfig.__init__(self)
        self._read_env()

    def _read_env(self):
        proxy = None
        proxies = urllib.getproxies()
        if not proxies:
            return

        for key, value in proxies.iteritems():
            proxy = proxies.get(key)
        user, authority = urllib.splituser(proxy)
        if authority:
            self.host, self.port = urllib.splitport(authority)
            self.active = True
        if user:
            self.username, self.password = urllib.splitpasswd(user)
            self.auth = True
        return

class Config(gobject.GObject):
    __gsignals__ = {
        'item-order-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'offline-mode-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'refresh-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'item-stored-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self, persistence, defaults):
        gobject.GObject.__init__(self)

        self._defaults = dict(defaults)
        self._options = dict(defaults)
        self.persistence = persistence

        self.initialize_options()
        self.initialize_proxy()

        self._apply_defaults()

    def _apply_defaults(self):
        for name, value in self._defaults.iteritems():
            if not name in self._options or self._options[name] == None:
                self._options[name] = value

    def initialize_proxy(self):
        # EnvironmentProxy has precedence
        self.proxy = EnvironmentProxyConfig()

        if not self.proxy.active:
            # .. defaults to GConfProxyConfig so we can listen for network
            # setting changes (e.g, changes in network preferences)
            self.proxy = GconfProxyConfig()

    def initialize_options(self):
        for key in self._options.keys():
            self._options[key] = self.persistence.load_option(key, type(self._defaults[key]))

    def get(self, key):
        if key in self._options and self._options[key] != None:
            return self._options[key]
        else:
            return self._defaults[key]

    def set(self, key, value):
        if self._options.has_key(key):# and self._options[key] != value:
            if value:
                value = type(self._defaults[key])(value)

            self.persistence.save_option(key, value)
            self._options[key] = value

def create_gconf_persistence():
    client = gconf.client_get_default()
    client.add_dir(GCONF_STRAW_ROOT, gconf.CLIENT_PRELOAD_ONELEVEL)
    return ConfigGConfPersistence(client)

def create_instance():
    defaults = config_options_defaults

    cp = ConfigPersistence((create_gconf_persistence(), defaults.keys()))
    config = Config(cp, defaults)

    return config

config_instance = None

def _get_instance():
    global config_instance

    if not config_instance:
        config_instance = create_instance()

    return config_instance

def get(key):
    config = _get_instance()
    return config.get(key)

def set(key, value):
    config = _get_instance()
    return config.set(key, value)

@property
def proxy():
    config = _get_instance()
    return config.proxy
