""" MessageManager.py

Manages status messages
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from dbus import DBusException
from subscribe import subscribe_show
import Config
import dbus
import dbus.glib
import dbus.service
import error
import gobject

log = error.get_logger()

# Based on the values from NetworkManager/include/NetworkManager.h
# We only care about CONNECTED and DISCONNECTED at the moment.
NM_STATE_CONNECTED = 3
NM_STATE_DISCONNECTED = 4

class FeedReader(dbus.service.Object):
    service_name = "org.gnome.feed.Reader"
    object_path = "/org/gnome/feed/Reader"

    def __init__(self):
        try:
            self._session_bus = dbus.SessionBus()
            self._service = dbus.service.BusName(self.service_name, bus=self._session_bus)
            dbus.service.Object.__init__(self, self._service, self.object_path)
        except DBusException, e:
            logging.info(_("Error while initializing feed subscribe service: [%s]" % str(e)))

    @dbus.service.method("org.gnome.feed.Reader")
    def Subscribe(self, url):
        subscribe_show(url)
        return True

class NetworkListener(object):
    SERVICE_NAME = "org.freedesktop.NetworkManager"
    SERVICE_PATH = "/org/freedesktop/NetworkManager"

    def __init__(self):
        pass

    def set_state(self, state):
        pass #self._config.offline = not (state == NM_STATE_CONNECTED)

    def active_cb(self, path):
        pass #self._config.offline = False

    def inactive_cb(self, path):
        pass #self._config.offline = True

def start_services():
    try:
        systemBus = dbus.SystemBus()
        proxy_obj = systemBus.get_object(NetworkListener.SERVICE_NAME, NetworkListener.SERVICE_PATH)
        nl = NetworkListener()

        # don't touch offline if it has been previously set.
        #if not Config.get_instance().offline:
        #    nl.set_state(proxy_obj.state())

        nm_interface = dbus.Interface(proxy_obj, NetworkListener.SERVICE_NAME)
        nm_interface.connect_to_signal('DeviceNowActive', nl.active_cb)
        nm_interface.connect_to_signal('DeviceNoLongerActive', nl.inactive_cb)
    except DBusException, e:
        log.info(_("Unable to find NetworkManager service: [%s]" % str(e)))

class StatusMessageManager(gobject.GObject):

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self):
        gobject.GObject.__init__(self)
        self.__messages = []

    def post_message(self, message):
        self.__messages.append(message)
        self.emit('changed')

    def read_message(self):
        return self.__messages.pop(0)

    def number_of_messages(self):
        return len(self.__messages)

_smmanager = None
def post_status_message(text):
    global _smmanager
    if not _smmanager:
        get_status_manager()
    _smmanager.post_message(text)

def get_status_manager():
    global _smmanager
    if not _smmanager:
        _smmanager = StatusMessageManager()
    return _smmanager
