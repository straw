from straw.model import Category, Feed
import SQLiteStorage as Storage

class DAO(object):
    def __init__(self, storage):
        self.storage = storage
    
    def _get_indexed(self, entities, index_field):
        return dict([(getattr(entity, index_field), entity) for entity in entities])

    def _get_grouped(self, entities, field):
        groups = {}
        previous = None

        for entity in entities:
            current = getattr(entity, field)

            if previous != current:
                # we have new group, let's make a dict entry for it
                groups[current] = []
                previous = current

            if current != None:
                groups[current].append(entity)

        return groups

    def _is_inherited(self, clazz):
        return len(clazz.__bases__) == 1 and hasattr(clazz, "inheritance") and clazz.inheritance
    
    def _get_pdict_table(self, entity):
        if self._is_inherited(entity.__class__) and hasattr(entity.__class__.__bases__[0], "pdict_table"):
            return entity.__class__.__bases__[0].pdict_table
        elif hasattr(entity.__class__, "pdict_table"):
            return entity.__class__.pdict_table
        else:
            return None

    def _get_persistent_fields(self, clazz):
        fields = list(clazz.persistent_properties)

        if self._is_inherited(clazz):
            fields.extend(clazz.__bases__[0].persistent_properties)

        return fields
    
    def tx_begin(self):
        self.storage._tx_begin()
    
    def tx_commit(self):
        self.storage._tx_commit()

    def query(self, sql):
        return self.storage.query(sql)

    def save(self, entity):
        do_insert = (entity.id == None)
        pdict_table = None

        if self._is_inherited(entity.__class__):
            base_class = entity.__class__.__bases__[0]
            base_data = dict([(property, getattr(entity, property)) for property in base_class.persistent_properties if property != base_class.primary_key])

            if hasattr(base_class, "pdict_table"):
                pdict_table = base_class.pdict_table

            if do_insert:
                id = self.storage.insert(base_class.persistent_table, base_data)
                entity.id = id
            else:
                self.storage.update(base_class.persistent_table, entity.id, base_data)
        else:
            if hasattr(entity.__class__, "pdict_table"):
                pdict_table = entity.__class__.pdict_table

        data = dict([(property, getattr(entity, property)) for property in entity.__class__.persistent_properties if property != entity.__class__.primary_key])

        if do_insert:
            id = self.storage.insert(entity.__class__.persistent_table, data)

            if not entity.id:
                entity.id = id
        else:
            self.storage.update(entity.__class__.persistent_table, entity.id, data)

        if pdict_table:
            self.save_pdict(entity)

        return entity.id

    def save_pdict(self, entity):
        pdict_table = self._get_pdict_table(entity)

        if not pdict_table:
            return None

        current_pdict = self.load_pdict(entity)
        new_pdict = entity.pdict

        for key in new_pdict.keys():
            if not new_pdict[key]:
                del new_pdict[key]
                continue

            if not current_pdict.has_key(key):
                self.storage.insert(pdict_table, { "entity_id": entity.id,
                                                  "entry_key": key,
                                                  "entry_value": new_pdict[key] })
            else:
                self.storage.update_with_where(pdict_table, "WHERE entity_id = %d AND entry_key = '%s'"
                                                % (entity.id, key),
                                                { "entry_key": key, "entry_value": new_pdict[key] })
                del current_pdict[key]

        # Now we need to remove entries that are not in new_pdict = has been deleted by the user.

        for key in current_pdict.keys():
            self.storage.query("DELETE FROM %s WHERE entity_id = %d AND entry_key = '%s'"
                               % (pdict_table, entity.id, key))

    def load_pdict(self, entity):
        pdict_table = self._get_pdict_table(entity)

        if not pdict_table:
            return None

        result = self.storage.query("SELECT entry_key, entry_value FROM %s WHERE entity_id = ?" % pdict_table, params = (entity.id,))

        return dict([(entry["entry_key"], entry["entry_value"]) for entry in result])

    def get_one(self, clazz, id):
        result = self.get(clazz, " WHERE id = %s" % id)
        return result[0]

    def get(self, clazz, sql = "", query = None, params = []):
        #import time
        #s = time.time()
        if not query:
            result = self.storage.query("SELECT * FROM %s%s" % (clazz.persistent_table, sql), params)
        else:
            result = self.storage.query(query, params)

        fields = clazz.persistent_properties

        if len(result) > 0:
            fields = filter(lambda x: x in fields, result[0].keys())

        entities = []

        #print "query took %s" % (time.time() - s)
        
        #s = time.time()

        for row in result:
            entity = clazz()
            [setattr(entity, field, row[field]) for field in fields]
            entities.append(entity)

        #print "hydration took %s" % (time.time() - s)

        return entities

    def get_indexed(self, clazz, index_field, sql = ""):
        result = self.storage.query("SELECT * FROM %s%s" % (clazz.persistent_table, sql))
        entities = {}

        for row in result:
            entity = clazz()

            i = 0
            for field in clazz.persistent_properties:
                setattr(entity, field, row[i])
                i += 1

            entities[getattr(entity, index_field)] = entity

        return entities

    def get_nodes(self, sql = ""):
        #result = self.storage.query("SELECT f.*, feed_id, unread_count FROM feeds f LEFT JOIN (SELECT COUNT(*) AS unread_count, feed_id FROM items i WHERE i.is_read = 0 GROUP BY i.feed_id) ON feed_id = f.id ORDER BY category_id")
        result = self.storage.query("SELECT *, unread_count FROM nodes n LEFT JOIN feeds f ON (f.id = n.id) LEFT JOIN categories c ON (c.id = n.id) LEFT JOIN (SELECT COUNT(*) AS unread_count, feed_id FROM items i WHERE i.is_read = 0 GROUP BY i.feed_id) ON feed_id = f.id ORDER BY n.parent_id, n.norder")
        entities = []

        for row in result:
            if row["type"] == "C":
                clazz = Category
            elif row["type"] == "F":
                clazz = Feed

            fields = self._get_persistent_fields(clazz)
            entity = clazz()
            entity.unread_count = 0

            for field in fields:
                value = row[field]
                #if row[field] == None:
                #    value = 0

                setattr(entity, field, value)

            if row["unread_count"] != None:
                setattr(entity, "unread_count", row["unread_count"])

            entity.pdict = self.load_pdict(entity)

            entities.append(entity)

        return self._get_indexed(entities, "id")

    def delete_category(self, id):
        self.storage.query("DELETE FROM categories WHERE id = ?", (id,))
        self.storage.query("DELETE FROM node_pdict_entries WHERE entity_id = ?", (id,))
        self.storage.query("DELETE FROM nodes WHERE id = ?", (id,))

    def delete_feed(self, id):
        self.storage.query("DELETE FROM items WHERE feed_id = ?", (id,))
        self.storage.query("DELETE FROM feeds WHERE id = ?", (id,))
        self.storage.query("DELETE FROM node_pdict_entries WHERE entity_id = ?", (id,))
        self.storage.query("DELETE FROM nodes WHERE id = ?", (id,))
