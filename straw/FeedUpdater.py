""" FeedUpdater.py

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

from JobManager import Job, TaskResult, TaskThread, JobHandler
import Config
import Fetcher
import JobManager
import SummaryParser
import gobject
import httplib2
import os
import urlparse

class FeedUpdateJobHandler(JobHandler):
    job_id = "feed-update"

    __gsignals__ = {
        "update-started" : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "update-done" : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
    }

    def __init__(self, id, job):
        JobHandler.__init__(self, id, job)

    def _on_fetch_started(self, handler, task):
        self._notify("update-started", task.user_data)

    def _on_url_fetched(self, handler, fetch_result):
        feed = fetch_result.task.user_data
        update_result = None

        if fetch_result.result.error:
            update_result = FeedUpdateResult("fetch-error", feed, fetch_result = fetch_result.result)
        else:
            try:
                feed = SummaryParser.parse(fetch_result.result.content, feed)
                update_result = FeedUpdateResult(None, feed, fetch_result = fetch_result.result)
            except Exception, e:
                update_result = FeedUpdateResult("parse-error", feed,
                                                 fetch_result = fetch_result.result, parse_error = e)

        self._notify("update-done", update_result)

    def _on_fetch_done(self, handler, data):
        pass

    def _prepare(self):
        self.fetch_tasks = []

        for feed in self.job.tasks:
            credentials = None

            if feed.pdict.has_key("username"):
                credentials = feed.pdict["username"], feed.pdict["password"], ""

            task = Fetcher.create_task(url = feed.location, credentials = credentials, user_data = feed)

            self.fetch_tasks.append(task)

        self.observers = [{ "task-done": [ self._on_url_fetched ],
                      "task-start": [ self._on_fetch_started ],
                      "job-done": [ self._on_fetch_done ]}]        

    def _run(self):
        self.fetcher_id = Fetcher.fetch(self.fetch_tasks, observers = self.observers, wait = True)

class FeedUpdateResult(object):
    def __init__(self, error, feed, fetch_result = None, parse_error = None):
        self.error = error
        self.feed = feed
        self.fetch_result = fetch_result

JobManager.register_handler(FeedUpdateJobHandler)

def update(feeds, observers):
    update = Job("feed-update")
    update.tasks = feeds
    update.observers = observers
    return JobManager.start(update)

def stop(id):
    JobManager.stop(id)
