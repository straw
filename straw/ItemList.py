""" ItemList.py

Handles listing of feed items in a view (i.e. GTK+ TreeView)
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """GNU General Public License

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

from Constants import *
from straw import helpers
from xml.sax import saxutils
import Config
import ItemManager
import MVP
import error
import gobject
import gtk
import os.path, operator
import pango
import pygtk
import time
pygtk.require('2.0')

class Column:
    """
    Constants for the item list treeview columns
    """
    TITLE, PUB_DATE, STICKY, ITEM, WEIGHT, STICKY_FLAG, FOREGROUND = range(7)

class ItemListModel:
    ''' A model of summary items for tree views '''

    def __init__(self):
        self.store = gtk.ListStore(str, str, gobject.TYPE_OBJECT, gobject.TYPE_PYOBJECT, int, bool, str)

    def populate(self, items):
        self.store.clear()

        if not items:
            return
        i = 0

        for item in items:
            weight = (pango.WEIGHT_BOLD, pango.WEIGHT_NORMAL)[item.is_read]
            #weight = pango.WEIGHT_NORMAL
            #colour = ("#0000FF", "#000000")[item.is_read]
            colour = "#000000"
            treeiter = self.store.append()
            #print self.store[i]
            
            self.store.set(treeiter,
                            Column.TITLE, item.title,
                            Column.PUB_DATE, item.pub_date,
                            Column.ITEM, item,
                            Column.WEIGHT, weight,
                            Column.STICKY_FLAG, False,
                            Column.FOREGROUND, colour)

            item.connect("notify", self.item_changed_cb)
            i += 1

    def item_changed_cb(self, item, property):
        row = [row for row in self.store if row[Column.ITEM] is item]

        if len(row) == 0:
            return

        row[0][Column.WEIGHT] = (pango.WEIGHT_BOLD, pango.WEIGHT_NORMAL)[item.is_read]

    @property
    def model(self):
        return self.store

class ItemListView(MVP.WidgetView):

    def _initialize(self):
        self._widget.set_rules_hint(False)
        self._widget.connect("button_press_event",self._on_button_press_event)
        self._widget.connect("popup-menu", self._on_popup_menu)
        self._widget.connect("row-activated", self._on_row_activated)
        uifactory = helpers.UIFactory('ItemListActions')
        action = uifactory.get_action('/itemlist_popup/mark_as_unread')
        action.connect('activate', self.on_menu_mark_as_unread_activate)
        self.popup = uifactory.get_popup('/itemlist_popup')

        renderer = gtk.CellRendererToggle()
        column = gtk.TreeViewColumn(_('Keep'), renderer,
                                    active=Column.STICKY_FLAG)
        column.set_resizable(True)
        column.set_reorderable(True)
        self._widget.append_column(column)
        renderer.connect('toggled', self._sticky_toggled)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('_Date'), renderer,
                                    text=Column.PUB_DATE,
                                    weight=Column.WEIGHT)
        column.set_resizable(True)
        column.set_reorderable(True)
        self._widget.append_column(column)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('_Title'), renderer,
                                    text=Column.TITLE,
                                    foreground=Column.FOREGROUND,
                                    weight=Column.WEIGHT)
        column.set_resizable(True)
        column.set_reorderable(True)
        self._widget.append_column(column)

        selection = self._widget.get_selection()
        selection.set_mode(gtk.SELECTION_SINGLE)
        # selection.connect('changed', lambda x: self.item_mark_as_read(True))

    def add_selection_changed_listener(self, listener):
        selection = self._widget.get_selection()
        selection.connect('changed',
                          listener.itemlist_selection_changed,
                          Column.ITEM)

    def _model_set(self):
        self._widget.set_model(self._model.model)

    def _sticky_toggled(self, cell, path):
        model = self._widget.get_model()
        treeiter = model.get_iter((int(path),))
        item = model.get_value(treeiter, Column.ITEM)
        item.sticky = not item.sticky
        model.set(treeiter, Column.STICKY_FLAG, item.sticky)

    def _on_popup_menu(self, treeview, *args):
        self.popup.popup(None,None,None,0,0)

    def _on_button_press_event(self, treeview, event):
        val = 0
        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = gtk.get_current_event_time()
            path = treeview.get_path_at_pos(x, y)
            if path is None:
                return 1
            path, col, cellx, celly = path
            treeview.grab_focus()
            treeview.set_cursor( path, col, 0)
            self.popup.popup(None, None, None, event.button, time)
            val = 1
        return val

    def on_menu_mark_as_unread_activate(self, *args):
        self.item_mark_as_read(False)

    def item_mark_as_read(self, isRead):
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter: return
        item = model.get_value(treeiter, Column.ITEM)

        item.props.is_read = isRead
        
        if isRead:
            weight = pango.WEIGHT_NORMAL
        else:
            weight = pango.WEIGHT_BOLD
        #colour = ("#0000FF", "#000000")[item.is_read]
        colour = "#000000"
        model.set(treeiter, Column.FOREGROUND, colour,
                  Column.WEIGHT, weight,
                  Column.ITEM, item)

    def mark_all_as_read(self):
        for row in self._widget.get_model():
            row[Column.WEIGHT] = pango.WEIGHT_NORMAL

    def _on_row_activated(self, treeview, path, column):
        ''' double-clicking an item opens that item in the web browser '''
        model = self._widget.get_model()
        try:
            treeiter = model.get_iter(path)
        except ValueError:
            return
        link = model.get_value(treeiter, Column.ITEM).link
        if link:
            helpers.url_show(link)
        return

    def select_first_item(self):
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()

        if not treeiter:
            return False

        path = model.get_path(model.get_iter_first())
        selection.select_path(path)
        return True

    def select_previous_item(self):
        """
        Selects the item before the current selection. If there
        is no current selection, selects the last item. If there is no
        previous row, returns False.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter: return False
        path = model.get_path(treeiter)
        prev = path[-1]-1
        if prev < 0:
            return False
        selection.select_path(prev)
        return True

    def select_next_item(self):
        """
        Selects the item after the current selection. If there is no current
        selection, selects the first item. If there is no next row, returns False.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter:
            treeiter = model.get_iter_first()
        next_iter = model.iter_next(treeiter)
        if not next_iter or not model.iter_is_valid(treeiter):
            return False
        next_path = model.get_path(next_iter)
        selection.select_path(next_path)
        return True

    def select_last_item(self):
        """
        Selects the last item in this list.
        """
        selection = self._widget.get_selection()
        selection.select_path(len(self.model.model) - 1)
        return True

    def select_next_unread_item(self):
        """
        Selects the first unread item after the current selection. If there is
        no current selection, or if there are no unread items, returns False..

        By returning False, the caller can either go to the next feed, or go
        to the next feed with an unread item.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        # check if we have a selection. if none,
        # start searching from the first item
        if not treeiter:
            treeiter = model.get_iter_first()
        if not treeiter: return False
        nextiter = model.iter_next(treeiter)
        if not nextiter: return False # no more rows to iterate
        treerow = model[treeiter]
        has_unread = False
        while(treerow):
            item = treerow[Column.ITEM]
            if not item.is_read:
                has_unread = True
                selection.select_path(treerow.path)
                break
            treerow = treerow.next
        return has_unread

class ItemListPresenter(MVP.BasicPresenter):

    ## XXX listen for RefreshFeedDisplaySignal, FeedOrder changes,

    def _initialize(self):
        self.model = ItemListModel()
        self.presented_nodes = []

    def populate(self, items):
        self.model.populate(items)
        self.view.set_model(self.model)

    def feedlist_selection_changed(self, feedlist_selection, column):
        (model, pathlist) = feedlist_selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]

        if not iters:
            return

        nodes = [model.get_value(treeiter, column) for treeiter in iters]

        if nodes == self.presented_nodes:
            # Selection hasn't actually changed so let's ignore the signal
            return

        self.presented_nodes = nodes

        items = []
        node = nodes[0]

        if node.node.type == "F":
            items = ItemManager.get_feed_items(node.node.id)
            self.view.set_model(None)
            self.model.populate(items)
            self.view.set_model(self.model)
        elif node.node.type == "C":
            self.view.set_model(None)
            items = ItemManager.get_category_items(node.node.id, self.populate)

        if not self.view.select_next_unread_item():
            self.view.select_first_item()

    def on_mark_all_as_read(self, action):
        self.view.mark_all_as_read()

    def flatten(self, sequence):
        ''' python cookbook recipe 4.6 '''
        for item in sequence:
            if isinstance(item, (list, tuple)):
                for subitem in self.flatten(item):
                    yield subitem
            else:
                yield item

    def select_previous_item(self):
        return self.view.select_previous_item()

    def select_next_item(self):
        return self.view.select_next_item()

    def select_next_unread_item(self):
        return self.view.select_next_unread_item()

    def select_last_item(self):
        return self.view.select_last_item()
