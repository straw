from gobject import GObject, SIGNAL_RUN_FIRST
from straw import FeedManager
from threading import Thread
import gobject
import os
import time

TEST_DB_PATH = "test.db"
TEST_OPML_PATH = "data/default_subscriptions.opml"
#TEST_OPML_PATH = "data/test.opml"
#TEST_OPML_PATH = "/home/ppawel/feedlist.opml"

class TestFeedManager(object):
    def setUp(self):
        FeedManager.setup(storage_path = TEST_DB_PATH)
        FeedManager.init() 

    def tearDown(self):
        os.remove(TEST_DB_PATH)

    def testCategories(self):
        FeedManager.save_category("test")

        for i in xrange(100):
            FeedManager.save_category("subtest", pid = (i + 1))

        tree = FeedManager.get_category_tree()

        assert tree[0].children[0].name == "subtest"
        assert tree[0].children[0].children[0].children[0].name == "subtest"

    def testImportOPML(self):
        dummy = DummyStraw("import_opml")
        dummy.start()
        dummy.join()

        feeds = FeedManager.get_feeds()
        print len(feeds)
        assert feeds != None

    def testUpdateAllFeeds(self):
        FeedManager.import_opml(TEST_OPML_PATH)
        
        time.sleep(1)
        
        dummy = DummyStraw("update_all_feeds")
        dummy.start()
        dummy.join()

        #feeds = FeedManager.upda()
        #print len(feeds)
        #assert feeds != None

class DummyStraw(Thread):
    def __init__(self, action, observers = None):
        Thread.__init__(self)
        self.action = action
        self.observers = observers

    def run(self):
        sleep_time = 2

        if self.action == "import_opml":
            FeedManager.import_opml(TEST_OPML_PATH)
        elif self.action == "update_all_feeds":
            FeedManager.update_all_feeds(self.observers)
            sleep_time = 5

        # processing happens in the background so we have to emulate
        # real application GUI loop by sleeping

        time.sleep(sleep_time)

class DummyObserver(GObject):
    def __init__(self):
        gobject.GObject.__init__(self)
        pass
    
    def handle(self, object, arg1):
        print "HANDLE!"
        print object
        print arg1