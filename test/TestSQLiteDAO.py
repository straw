from straw.model import Feed, Item
from straw.storage import DAO, Storage
import nose
import os

TEST_DB_PATH = "test.db"

class TestSQLiteDAO:
    def setUp(self):
        self.storage = Storage(TEST_DB_PATH)
        self.dao = DAO(self.storage)

    def tearDown(self):
        os.remove(TEST_DB_PATH)
        
    def _create_test_feed(self):
        feed = Feed()
        feed.title = "test feed title"
        return feed
    
    def _create_test_item(self, feed):
        item = Item()
        item.title = "test feed title"
        item.feed_id = feed.id
        return item

    def testSaveFeeds(self):
        feeds = []
        for i in xrange(100):
            feed = self._create_test_feed()
            feeds.append(feed)
            self.dao.save(feed)

        i = 0
        for feed in feeds:
            assert feed.id != None
            i += 1
            feed.title = "bla bla %s" % (i)
            self.dao.save(feed)

        i = 0
        for feed in feeds:
            i += 1
            assert feed.title == "bla bla %s" % (i)
            
    def testSaveItems(self):
        items = []
        feeds = []

        for i in xrange(10):
            feed = self._create_test_feed()
            feeds.append(feed)
            self.dao.save(feed)
            
            for i in xrange(10):
                item = self._create_test_item(feed)
                items.append(item)
                self.dao.save(item)

        i = 0
        for item in items:
            assert item.id != None
            i += 1
            item.title = "item bla bla %s" % (i)
            self.dao.save(item)

        i = 0
        for item in items:
            i += 1
            assert item.title == "item bla bla %s" % (i)

    def testListFeeds(self):
        feed = self._create_test_feed()
        self.dao.save(feed)
        assert len(self.dao.list_all()) > 0

    def testListItems(self):
        feed = self._create_test_feed()
        self.dao.save(feed)
        item = self._create_test_item(feed)
        self.dao.save(item)
        assert len(self.dao.get(Item)) > 0
