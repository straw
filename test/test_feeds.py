
import pygtk
import gobject
from mock import Mock
import nose

import os

from straw.feeds import FifoCache

"""
class TestSubscription:

    def cb(self, *args):
        ''' default callback that asserts that it's been called'''
        assert True # callback called

    def setUp(self):
        ''' setup subscription testing '''
        self.subscription = Subscription()

    def tearDown(self):
        ''' tear down tests '''
        self.subscription = None

    def test_add_feed_into_default_category(self):
        ''' Test adding of a feed subscription '''
        feed = Feed('Test Feed','http://test.com/feed')
        self.subscription.connect('feed-added',self.cb)
        self.subscription.add(feed, category=None)
        assert feed in self.subscription.feeds # XXX why? assuming that subscription is a 'feed list'?

    def test_get_feed_without_category(self):
        ''' Test retrieving a feed that is not part of any category '''
        feed = Feed('TestFeed','http://test.com/feed')
        feed.id = 255
        self.subscription.add(feed, category=None)
        f = self.subscription.get('http://test.com/feed')
        assert f == feed

    def test_get_feed_with_category(self):
        ''' Test retrieving a feed that is part of a category '''
        feed = Feed('TestFeed','http://test.com/feed')
        feed.id = 255
        category = self.subscription.get_category('CategoryPlanet')
        self.subscription.add(feed, category=category)
        f = self.subscription.get('http://test.com/feed')
        assert f == feed

    def test_add_category_empty(self):
        ''' Add an empty category, a category with no attached feeds '''
        category = Category('Empty Category')
        self.subscription.connect('category-added', self.cb)
        self.subscription.add_category(category) ## XXX add_foo sucky API?
        assert category in self.subscription.categories

    #def test_add_category_with_feeds(self):
    #    ''' i.e., most likely an OPML subscription '''
    #    pass

    def test_get_category(self):
        ''' Test category retrieval '''
        catA = Category('NewCategory')
        self.subscription.add_category(catA)
        catB = self.subscription.get_category('NewCategory')
        assert catA == catB

    def test_add_feed_in_category(self):
        ''' test adding a feed that will be part of a new category '''
        def _cb(category, feed):
            assert feed in category
        feed = Feed('Test Feed', 'http://test.com/feed')
        category = self.subscription.get_category('CategoryPlanet')
        self.subscription.connect('category-feed-added', _cb)
        self.subscription.add(feed, category=category)
        assert feed in category
        assert feed not in self.subscription.uncategorized

    def test_remove_feed_out_of_category(self):
        ''' test moving a feed out of a category '''
        feed = Feed('Test','http://test.com/feed')
        category = Category('New')
        self.subscription.add_category(category)
        self.subscription.add(feed,category)
        self.subscription.remove(feed, category)
        assert feed not in category
        assert feed in self.subscription.uncategorized

    def test_remove_category_empty(self):
        ''' test removing an empty category '''
        category = Category('New')
        self.subscription.add_category(category)
        self.subscription.remove_category('New')
        assert category not in self.subscription.categories

    def test_remove_category_with_feeds(self):
        ''' remove a category with feeds '''
        category = Category('New')


    def test_remove_feed_with_category(self):
        ''' test removing a feed that's part of a category '''
        pass

    def test_remove_feed_no_category(self):
        ''' test removing a feed that is not part of a category '''
        pass
"""

class TestFifoCache:

    def setUp(self):
        self.cache = FifoCache(num_entries=20)

    def tearDown(self):
        self.cache = None

    def test_feed_update_info(self):
        ''' test that updating a feed's metadata works and notifications
        are working properly.'''
        for i,x in enumerate(range(19)):
            self.cache[i] = x
        assert len(self.cache) == 19

    def test_feed_update_items(self):
        ''' test updating of feeds items from new items '''
        pass

    def test_feed_restore_items(self):
        ''' test item restoration from an external source (e.g., database,
        etc...)'''
        pass


    def test_init(self):
        ''' Test initialization process (e.g. loading of feeds, categories,
        and grouping/categorizing of feeds into different categories.'''
        pass
