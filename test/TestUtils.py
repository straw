import sys
sys.path.insert(0, '../src/lib')

import straw.utils

class TestUtils:

    def testFormatDateBadEncoding(self):
        # Bugzilla 145513
        encoding = "en_US.ISO8859-1"
        format = "%A"
        time = (2004, 8, 5, 20, 29, 59, 3, 218, 0)
        try:
            utils.format_date(time, format, encoding)
        except Exception, ex:
            self.fail("format_date: %s" % str(ex))

    def testURLlocation(self):
        url = "http://www.amazon.com/gp/aws/sdk/103-9053546-3314261"
        self.assertEqual("http://www.amazon.com", utils.get_url_location(url))

    def testReadText(self):
        text = "Blah &gt;foo&lt; <p>bar</p>"
        t = utils.read_text(text, 60)
        self.assertEqual(t, "Blah >foo< bar")

    def testCompleteUrl(self):
        tlist = [("images/foo.jpg","http://blah.com","http://blah.com/images/foo.jpg"),
                 ("/images/foo.jpg","http://b.com","http://b.com/images/foo.jpg")]
        for u,b,r in tlist:
            self.assertEqual(r,utils.complete_url(u,b))
