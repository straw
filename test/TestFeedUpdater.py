import straw.FeedUpdater as FeedUpdater
from gobject import GObject, SIGNAL_RUN_FIRST
import gobject

#from straw.FeedUpdater import FeedUpdateJobHandler
#from straw.JobManager import SimpleJobHandler, TestThreadPoolJobHandler
#import straw.JobManager as JobManager

class TestFeedUpdater(object):
    def testFeedUpdater(self):
        a = [ "http://rss.slashdot.org/Slashdot/slashdot", \
             "http://newsrss.bbc.co.uk/rss/sportonline_world_edition/tennis/rss.xml", \
             "http://news.com.com/2547-1_3-0-5.xml", \
             "http://feeds.wired.com/wired/topheadlines", \
             "http://www.theregister.co.uk/headlines.rss", \
             "http://feeds.feedburner.com/boingboing/iBag" ]

        aa = DummyObserver()
        observers = { 'job-done' : [aa.handle] }
        FeedUpdater.update_urls(a, observers)
        
        

class DummyObserver(GObject):
    def __init__(self):
        gobject.GObject.__init__(self)
        pass
    
    def handle(self, object, arg1):
        print "HANDLE!"
        print object
        print arg1