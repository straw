import unittest
import sys
import locale
sys.path.insert(0, "../src/lib")
import SummaryParser

try:
    import pmock
except ImportError:
    sys.exit("You need python-mock 0.3 to run this test. http://pmock.sf.net")


class TitleImgParserTest(unittest.TestCase):
    def setUp(self):
        self._parser = SummaryParser.TitleImgParser()

    def tearDown(self):
        self._parser.close()

    def testHTMLinTitle(self):
        fragment = "<p><a href=http://www.nongnu.org/straw>Straw</a>: <b>GNOME</b> <i>Desktop</i> Aggregator"
        self._parser.feed(fragment)
        self.assertEqual("Straw: GNOME Desktop Aggregator", self._parser.get_text())
        self._parser.close()
        fragment = "<span class=\"courtcase\">Lawrence</span>"
        self._parser.feed(fragment)
        self.assertEqual("Lawrence", self._parser.get_text())

    def testImgInHTML(self):
        fragment = "<p><img src='foo.jpg'/></p>"
        mock = pmock.Mock()
        mock.expects(pmock.at_least_once()).get_location().will(pmock.return_value('http://foo.com'))
        self._parser.set_feed(mock)
        self._parser.feed(fragment)
        mock.verify()
        self.assertEqual(["http://foo.com/foo.jpg"], self._parser.get_image_urls())
        fragment = "<a href='http://foo.com'><img src='/~jan/bar/foo/straw.png'></a>"
        self._parser.close()
        self._parser.feed(fragment)
        mock.verify()
        self.assertEqual(["http://foo.com/~jan/bar/foo/straw.png"], self._parser.get_image_urls())

    def testPlainTextInTitle(self):
        # Make sure plain text works too
        fragment = "Straw Rocks!"
        self._parser.feed(fragment)
        self.assertEqual("Straw Rocks!", self._parser.get_text())

    def testEntityInTitle(self):
        # http://bugzilla.gnome.org/show_bug.cgi?id=149924
        tdata =[("Joe &amp; Bob. One &lt; Two but &gt; 0","Joe & Bob. One < Two but > 0"),
                ("<b>Is<b> 2 &lt; <i>1<i>?\"foo\"","Is 2 < 1?\"foo\""),
                ("&lt;h2&gt;","<h2>")]

        for fragment,expected in tdata:
            self._parser.feed(fragment)
            self.assertEqual(expected, self._parser.get_text())
            self._parser.close()

    def testTitleLength(self):
        CHARS = 60
        # Excerpt from one of Jarno Virtannen's blog entries
        fragment = """
        I am not going to write much here for few weeks, because I
        <span class="bar">am taking</span> regular visits to a
        <a href="test.com">dentist</a>. I am not sure about the correct English term for my previous
        procedure, but I think it is called root canal treatment. The
        operation itself wasn't as painful as I was expecting (though not too
        pleasurable either) but I have to take antibiotics, which mess up my
        stomach and therefore I am not too energetic.
        """
        self._parser.feed(fragment)
        self.assertEqual(CHARS, len(self._parser.get_text(CHARS)))

    def testBrAndFontTagsInTitle(self):
        # http://bugzilla.gnome.org/show_bug.cgi?id=148105
        fragment="""
        <br><br><br><br><font color=444444 size=1>Shadow</font><br><br>
        """
        self._parser.feed(fragment)
        self.assertEqual("Shadow", self._parser.get_text())


def suite():
    return unittest.makeSuite(TitleParserTest, "test")

if __name__ == "__main__":
    unittest.main()
