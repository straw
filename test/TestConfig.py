# Test Config.py


import pygtk
import gobject
from mock import Mock
import nose

import os

from straw.Config import Config

class TestConfig:
    def setUp(self):
        self.mockPersistence = Mock( {
                'save_option': None,
                '__nonzero__' : 1
                })
        self.config = Config(self.mockPersistence)

    def tearDown(self):
        self.config = None
        self.mockPersistence = None

    def testProxy(self):
        os.environ['http_proxy'] = 'http://localhost:3128'
        self.config.initialize_proxy()
        assert isinstance(self.config.proxy, Config.EnvironmentProxyConfig)
        del os.environ['http_proxy']
        self.config.initialize_proxy()
        assert isinstance(self.config.proxy, Config.GconfProxyConfig)

    def testPollFrequency(self):
        def _cb(x): assert x is self.config
        f = 18000
        self.config.connect('refresh-changed', _cb)
        self.config.poll_frequency = f
        self.mockPersistence.expectParams(Config.OPTION_POLL_FREQUENCY, f)
        assert self.config.poll_frequency == f

    def testLastPoll(self):
        last_poll = 24600
        self.config.last_poll = last_poll
        self.mockPersistence.expectParams(Config.OPTION_LAST_POLL, last_poll)
        assert self.config.last_poll == last_poll

    def testItemsStored(self):
        def _cb(x): assert x is self.config
        stored = 150
        self.config.connect('items-stored-changed', _cb)
        self.config.number_of_items_stored = stored
        assert self.config.number_of_items_stored == stored

    def testItemOrder(self):
        def _cb(x): assert x is self.config
        order = True
        self.config.connect('item-order-changed', _cb)
        self.config.item_order = order
        assert self.config.item_order == order

    def testOffline(self):
        def _cb(x): assert x is self.config
        offline = False
        self.config.connect('offline-mode-changed', _cb)
        self.config.offline = offline
        assert self.config.offline == offline

    # feel free to add more tests here ...
