import unittest
import os
import sys

try:
    import pmock
except ImportError:
    sys.exit("You need python-mock (pmock, 0.3) to run this test. http://pmock.sf.net")

sys.path.insert(0, '../src/lib')
import subscribe

class FeedLocationPresenterTestCase(unittest.TestCase):
    def setUp(self):
        self.presenter = subscribe.FeedLocationPresenter()

    def testSplit_urlWithAbsoluteUrl(self):
        feed = "http://www.unpluggable.com/plans/log.rdf"
        url, username, password = self.presenter.split_url(feed)
        self.assertEqual(url, feed)

    def testSplit_urlWithAuthToken(self):
        feed = "http://foo:com@test.org/very/long/path/to/feed.xml"
        url, username, password = self.presenter.split_url(feed)
        self.assertEqual(url, "http://test.org/very/long/path/to/feed.xml")
        self.assertEqual("foo", username)
        self.assertEqual("com", password)

    def testUrlWithLeadingSpace(self):
        feed = "  http://www.test.org/rss10.xml "
        url, username, password = self.presenter.split_url(feed)
        self.assertEqual("http://www.test.org/rss10.xml", url)

    def testUrlSchemeNotHttp(self):
        feed = "file:///home/bart/simpson.xml"
        view = pmock.Mock()
        model = pmock.Mock()
        view.expects(pmock.at_least_once()).method("report_error")
        model.expects(pmock.at_least_once()).method("poll")
        self.presenter.view = view
        self.presenter.model = model
        url, uname, pword = self.presenter.split_url(feed)
        self.presenter.find_feed(url)
        try:
            view.verify()
        except Exception, ex:
            # view.report_error() should've been called since
            # 'file' is not currently supported
            self.fail(ex)

    def testDomainCheck(self):
        feed = "http://www.foo.org/blog/devel/atom.xml"
        self.presenter.split_url(feed)
        self.assertEqual("www.foo.org", self.presenter.get_feed_domain())

    def tearDown(self):
        self.presenter = None

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(FeedLocationPresenterTestCase, 'test'))
    #suite.addTest(unittest.makeSuite(FeedsPresenterTestCase, 'test'))
    unittest.TextTestRunner(verbosity=2).run(suite)
