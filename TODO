-----
STUFF
-----

* Problems

- sqlite date handling - apparently, sqlite accepts every string as data in TIMESTAMP/DATETIME
  columns (see http://www.sqlite.org/datatype3.html#affinity) so we have to implement date handling
  ourselves

* Thoughts

- Implement ItemLoader - spawns workers that load feed items incrementally (by 50/100/whatever
  items) - that would allow for smooth feed list navigation; obstacles to overcome here mainly
  involve UI stuff - how to handle not-yet-loaded parts of the item list?

----
TODO
----

* High priority

- JobManager: stopping a job = allow for graceful shutdown
- JobManager: consider using stack instead of FIFO queue to allow adding priority tasks
  when job is already running

- Feed error display
- Fix and migrate tray icon

* Lower priority (but would be nice to have!)

- Subscription Import Assistant - make importing more interactive: first select OPML,
  then parse OPML, show OPML contents + let the user choose which feeds exactly to import
  (default would be all) and where to put them; see also feed discovery dialog

- Tags - allow labeling feeds/feed items + UI for browsing tag contents

- Flexible item list - support for selecting/sorting/grouping criteria - then we get stuff like
  Search Folders (a.k.a. Virtual Folders, Saved Searches etc) for free!

-----
STUFF
-----

* Ideas for the future

- Don't keep everything in memory when it's in the db too.
    Update 1: Done for images
    Update v0.26: Loading and unloading of feed content on feed selection.
- Customization of the rendering, maybe allow for user-supplied CSS or at
  least some sort of customization dialogs.
- An interface to syndic8, bloglines, feedster, etc...
- Allow for dragging stuff from Straw too.
- Popup menu to html view
   - copying urls to clipboards [ DONE ]
   - saving images 
- Alternative views of the feeds/items, 2-pane and 3-pane views.
- More unit tests
- Article diffs: show the user why an article was marked as unread again

* The following aren't Straw bugs, but affect it:

- First item of a list getting selected when nothing is selected and
  user clicks on something:

    http://bugzilla.gnome.org/show_bug.cgi?id=82344 [RESOLVED DUPLICATE of 94837 (which is fixed)]

- Occasional repeatable segfault with certain items:
    
    http://bugzilla.gnome.org/show_bug.cgi?id=87490 [RESOLVED FIXED]
    http://bugzilla.gnome.org/show_bug.cgi?id=87567 [RESOLVED DUPLICATE of 79782 (which is fixed)]
