# Italian translation of Straw
# Traduzione italiana di Straw
# Copyright (C) 2002-2005 Juri Pakaste
# This file is distributed under the same license as the Straw package.
# Aldo "Xoen" Giambelluca <xoen@email.it>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: Straw 0.25.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-02-03 21:48+0100\n"
"PO-Revision-Date: 2005-01-29 08:03+0100\n"
"Last-Translator: Aldo  \"Xoen\" Giambelluca <xoen@email.it>\n"
"Language-Team: Italian <tp@lists.linux.it>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../glade/straw.glade.h:1
msgid "  "
msgstr "  "

#: ../glade/straw.glade.h:2
msgid "    "
msgstr "    "

#: ../glade/straw.glade.h:3
msgid "*"
msgstr "*"

#: ../glade/straw.glade.h:4
msgid "0 items found"
msgstr "nessun elemento trovato"

#: ../glade/straw.glade.h:5
msgid "<b>Authorization</b>"
msgstr "<b>Autorizzazione</b>"

#: ../glade/straw.glade.h:6
msgid "<b>Categories</b>"
msgstr "<b>Categorie</b>"

#: ../glade/straw.glade.h:7
msgid "<b>General</b>"
msgstr "<b>Generale</b>"

#: ../glade/straw.glade.h:8
msgid "<b>Polling and Article Storage</b>"
msgstr "<b>Aggiornamento e memorizzazione articolo</b>"

#: ../glade/straw.glade.h:9
#, fuzzy
msgid "<b>Refresh times</b>"
msgstr "<b>Tempi aggiornamento</b>"

#: ../glade/straw.glade.h:10
msgid "<b>Source Information</b>"
msgstr "<b>Informazioni sulla sorgente</b>"

#: ../glade/straw.glade.h:11
msgid ""
"<span weight=\"bold\">C_ategories\n"
"</span>"
msgstr ""
"<span weight=\"bold\">C_ategorie\n"
"</span>"

#: ../glade/straw.glade.h:13
#, fuzzy
msgid "<span weight=\"bold\">_External Source for Category Contents:</span>"
msgstr ""
"<span weight=\"bold\">_Sorgente esterna per i contenuti della categoria:</"
"span>"

#: ../glade/straw.glade.h:14
msgid "<span weight=\"bold\">_Sort Category Contents:</span>"
msgstr "<span weight=\"bold\">_Ordina i contenuti della categoria:</span>"

#: ../glade/straw.glade.h:15
#, fuzzy
msgid "Auth"
msgstr "Autorizzazione"

#: ../glade/straw.glade.h:16
msgid "Connecting to ..."
msgstr "Connessione a ..."

#: ../glade/straw.glade.h:17
msgid "Default _refresh frequency:"
msgstr "_Frequenza di aggiornamento predefinita:"

#: ../glade/straw.glade.h:18
msgid "Feed Copyright"
msgstr "Copyright feed"

#: ../glade/straw.glade.h:19
msgid "Feed Description"
msgstr "Descrizione feed"

#: ../glade/straw.glade.h:20
msgid "Feed Properties"
msgstr "Proprietà feed"

#: ../glade/straw.glade.h:21
msgid "Feed URL:"
msgstr "URL feed:"

#: ../glade/straw.glade.h:22
msgid "Limit earliest date"
msgstr "Limita data più vecchia"

#: ../glade/straw.glade.h:23
msgid "Limit latest date"
msgstr "Limita data più recente"

#: ../glade/straw.glade.h:24
msgid "Location:"
msgstr "Posizione:"

#: ../glade/straw.glade.h:25
msgid "Main"
msgstr "Principale"

#: ../glade/straw.glade.h:26
msgid "Mark All As _Read"
msgstr "_Segna tutti come letti"

#: ../glade/straw.glade.h:27
msgid "Multiple Feed"
msgstr "Feed multipli"

#: ../glade/straw.glade.h:28
msgid "N_ext Feed"
msgstr "Feed s_uccessivo"

#: ../glade/straw.glade.h:29
msgid "Ne_xt Feed with Unread"
msgstr "_Feed successivo non letto"

#: ../glade/straw.glade.h:30
msgid "Next Refresh:"
msgstr "Aggiornamento successivo:"

#: ../glade/straw.glade.h:31
msgid "Next _Category"
msgstr "Categoria s_uccessiva"

#: ../glade/straw.glade.h:32
msgid "P_revious Feed"
msgstr "Feed pr_ecedente"

#: ../glade/straw.glade.h:33
msgid "Password:"
msgstr "Password:"

#: ../glade/straw.glade.h:34
msgid "Previous Ca_tegory"
msgstr "Categoria pr_ecedente"

#: ../glade/straw.glade.h:35
msgid "Previous Refresh:"
msgstr "Aggiornamento precedente:"

#: ../glade/straw.glade.h:36
msgid "Progress"
msgstr "Avanzamento"

#: ../glade/straw.glade.h:37
msgid "Re_move Selected Feed"
msgstr "_Elimina feed selezionato"

#: ../glade/straw.glade.h:38
msgid "Refresh All"
msgstr "Aggiorna tutti"

#: ../glade/straw.glade.h:39
msgid "Refresh Cate_gory"
msgstr "Aggiorna cate_goria"

#: ../glade/straw.glade.h:40
msgid "Refresh _Selected"
msgstr "Aggiorna _selezionato"

#: ../glade/straw.glade.h:41
msgid "Refresh frequency:"
msgstr "Frequenza aggiornamento:"

#: ../glade/straw.glade.h:42
#, fuzzy
msgid "Scroll or Next Unread"
msgstr "Scorri al successivo non letto"

#: ../glade/straw.glade.h:43
msgid "Select the categories to which this feed should belong to."
msgstr "Selezionare le categorie alle quali questo feed dovrebbe appartenere."

#: ../glade/straw.glade.h:44
msgid "Select the feeds you want to subscribe to below:"
msgstr "Selezionare quì sotto i feed che si vogliono sottoscrivere:"

#: ../glade/straw.glade.h:45
msgid "Straw"
msgstr "Straw"

#: ../glade/straw.glade.h:46
msgid "Straw Preferences"
msgstr "Preferenze Straw"

#: ../glade/straw.glade.h:47 ../src/lib/SubscribeDialog.py:70
msgid "Subscribe"
msgstr "Sottoscrivi"

#: ../glade/straw.glade.h:48
msgid "Subscribe to feed"
msgstr "Sottoscrivi feed"

#: ../glade/straw.glade.h:49
msgid "Subscription _Properties"
msgstr "_Proprietà sottoscrizione"

#: ../glade/straw.glade.h:50
msgid "Us_e Default"
msgstr "U_sa predefinito"

#: ../glade/straw.glade.h:51
msgid "Use _Default"
msgstr "Usa _predefinito"

#: ../glade/straw.glade.h:52
msgid "Use this only if you need authorization to read this feed."
msgstr ""
"Usa questo solo se c'è bisogno di autorizzazione per leggere questo feed."

#: ../glade/straw.glade.h:53
msgid "User name:"
msgstr "Nome utente:"

#: ../glade/straw.glade.h:54
msgid "Username:"
msgstr "Nome utente:"

#: ../glade/straw.glade.h:55
msgid "_Advance Search"
msgstr "_Ricerca avanzata"

#: ../glade/straw.glade.h:56
msgid "_Articles:"
msgstr "_Articoli:"

#: ../glade/straw.glade.h:57
msgid "_Authorization"
msgstr "_Autorizzazione"

#: ../glade/straw.glade.h:58
msgid "_Categories"
msgstr "_Categorie"

#: ../glade/straw.glade.h:59
msgid "_Default number of articles stored per feed:"
msgstr "_Numero predefinito di articoli memorizzati per feed:"

#: ../glade/straw.glade.h:60
msgid "_Export Subscriptions"
msgstr "_Esporta sottoscrizioni"

#: ../glade/straw.glade.h:61
msgid "_General"
msgstr "_Generale"

#: ../glade/straw.glade.h:62
msgid "_Go"
msgstr "_Vai"

#: ../glade/straw.glade.h:63
msgid "_Import Subscriptions"
msgstr "_Importa sottoscrizioni"

#: ../glade/straw.glade.h:64
msgid "_Information"
msgstr "_Informazioni"

#: ../glade/straw.glade.h:65
msgid "_Location of new items:"
msgstr "_Posizione dei nuovi elementi:"

#: ../glade/straw.glade.h:66
msgid "_Location:"
msgstr "_Posizione:"

#: ../glade/straw.glade.h:67
msgid "_Next"
msgstr "S_uccessivo"

#: ../glade/straw.glade.h:68
msgid "_Next Refresh:"
msgstr "Aggiornamento s_uccessivo:"

#: ../glade/straw.glade.h:69
msgid "_Number of articles stored:"
msgstr "_Numero di articoli memorizzati:"

#: ../glade/straw.glade.h:70
msgid "_Password:"
msgstr "_Password:"

#: ../glade/straw.glade.h:71
msgid "_Previous"
msgstr "Pr_ecedente"

#: ../glade/straw.glade.h:72
msgid "_Refresh frequency (minutes):"
msgstr "_Frequenza aggiornamento (minuti):"

#: ../glade/straw.glade.h:73
msgid "_Scroll or Next Unread"
msgstr "_Scorri al successivo non letto"

#: ../glade/straw.glade.h:74
msgid "_Search items for:"
msgstr "_Cerca elementi per:"

#: ../glade/straw.glade.h:75
msgid "_Subscribe"
msgstr "_Sottoscrivi"

#: ../glade/straw.glade.h:76
msgid "_Subscriptions:"
msgstr "_Sottoscrizioni:"

#: ../glade/straw.glade.h:77
msgid "_Title:"
msgstr "_Titolo:"

#: ../glade/straw.glade.h:78
msgid "_Username:"
msgstr "_Nome utente:"

#: ../glade/straw.glade.h:79
msgid "articles"
msgstr "articoli"

#: ../glade/straw.glade.h:80
msgid "bottom"
msgstr "basso"

#: ../glade/straw.glade.h:81
msgid "minutes"
msgstr "minuti"

#: ../glade/straw.glade.h:82
msgid "top"
msgstr "alto"

#: ../src/eggtray/eggtrayicon.c:117
msgid "Orientation"
msgstr "Orientamento"

#: ../src/eggtray/eggtrayicon.c:118
msgid "The orientation of the tray."
msgstr "L'orientamento della tray."

#. have to define this here so the titles can be translated
#: ../src/lib/FeedCategoryList.py:23
msgid "All"
msgstr "Tutti"

#: ../src/lib/FeedCategoryList.py:24
msgid "Uncategorized"
msgstr "Senza categoria"

#: ../src/lib/FeedPropertiesDialog.py:67
msgid "Member"
msgstr "Membro"

#: ../src/lib/FeedPropertiesDialog.py:71
msgid "Name"
msgstr "Nome"

#: ../src/lib/FeedPropertiesDialog.py:122
#, python-format
msgid "%s Properties"
msgstr "Proprietà di %s"

#: ../src/lib/Find.py:136
#, python-format
msgid "%d items found"
msgstr "%d elementi trovati"

#: ../src/lib/ItemStore.py:80
msgid "There was a problem while converting the database"
msgstr "Si è verificato un problema durante la conversione del database"

#: ../src/lib/ItemStore.py:81
#, python-format
msgid ""
"\n"
"                Straw will not behave as excepted, you should probably quit "
"now.\n"
"                The exception has been saved to the file %s. Please see\n"
"                the Straw README for further instructions.\n"
"                "
msgstr ""
"\n"
"                Straw non si comporterà come previsto, bisognerebbe uscire "
"ora.\n"
"                L'eccezione è stata salvata nel file %s. Vedere\n"
"                il README di Straw per ulteriori istruzioni.\n"
"                "

#: ../src/lib/ItemStore.py:385
msgid "Couldn't import mx.DateTime"
msgstr "Impossibile importare mx.DateTime"

#: ../src/lib/MainWindow.py:91
msgid "Error Loading Browser"
msgstr "Errore nel caricamento del browser"

#: ../src/lib/MainWindow.py:92
msgid ""
"Straw cannot find a browser to view this item. Please check your browser "
"settings and try again."
msgstr ""
"Impossibile trovare un browser per visualizzare questo elemento. Controllare "
"le impostazioni del browser e riprovare."

#: ../src/lib/MainWindow.py:174
msgid "No data yet, need to poll first."
msgstr "Nessun dato ancora, è necessario aggiornare prima."

#: ../src/lib/MainWindow.py:183
msgid "Title:"
msgstr "Titolo:"

#: ../src/lib/MainWindow.py:195
msgid "Date:"
msgstr "Data:"

#: ../src/lib/MainWindow.py:206
msgid "Feed:"
msgstr "Feed:"

#: ../src/lib/MainWindow.py:222
msgid "Publication"
msgstr "Pubblicazione"

#: ../src/lib/MainWindow.py:225
msgid "Volume"
msgstr "Volume"

#: ../src/lib/MainWindow.py:230
msgid "Section"
msgstr "Sezione"

#: ../src/lib/MainWindow.py:233
msgid "Starting Page"
msgstr "Pagina iniziale"

#: ../src/lib/MainWindow.py:241
msgid "Software license"
msgstr "Licenza software"

#: ../src/lib/MainWindow.py:244
msgid "Changes"
msgstr "Cambiamenti"

#: ../src/lib/MainWindow.py:254
msgid "Posted by"
msgstr "Inviato da"

#: ../src/lib/MainWindow.py:258
msgid "Contributor:"
msgstr "Collaboratore:"

#: ../src/lib/MainWindow.py:260
msgid "Permalink"
msgstr "Link permanente"

#: ../src/lib/MainWindow.py:266
msgid "Complete story"
msgstr "Storia completa"

#: ../src/lib/MainWindow.py:269
msgid "License"
msgstr "Licenza"

#: ../src/lib/MainWindow.py:272
msgid "Additional information"
msgstr "Informazioni aggiuntive"

#: ../src/lib/MainWindow.py:325
msgid "/Mark as _Unread"
msgstr "/Segna come _non letto"

#: ../src/lib/MainWindow.py:347
msgid "Keep"
msgstr "Mantieni"

#: ../src/lib/MainWindow.py:355 ../src/lib/SubscribeDialog.py:75
msgid "Title"
msgstr "Titolo"

#: ../src/lib/MainWindow.py:530
msgid "Straw is currently offline. Click to go online."
msgstr "Straw è attualmente offline. Clicca per andare online."

#: ../src/lib/MainWindow.py:534
msgid "Straw is currently online. Click to go offline."
msgstr "Straw è attualmente online. Clicca per andare offline."

#: ../src/lib/MainWindow.py:564
msgid "Refresh all feeds"
msgstr "Aggiorna tutti i feed"

#: ../src/lib/MainWindow.py:565
msgid "Subscribe to a new feed"
msgstr "Sottoscrivi nuovo feed"

#: ../src/lib/MainWindow.py:566
msgid "Find text in articles"
msgstr "Cerca testo negli articoli"

#: ../src/lib/MainWindow.py:567
msgid "Scroll article or go to next unread article"
msgstr "Scorri articolo o vai al successivo articolo non letto"

#: ../src/lib/MainWindow.py:769
msgid "/_Refresh"
msgstr "/_Aggiorna"

#: ../src/lib/MainWindow.py:770
msgid "/_Stop Refresh"
msgstr "/_Ferma aggiornamento"

#: ../src/lib/MainWindow.py:771
msgid "/_Mark as Read"
msgstr "/Segna come _letto"

#: ../src/lib/MainWindow.py:772
msgid "/sep"
msgstr ""

#: ../src/lib/MainWindow.py:773
msgid "/_Remove"
msgstr "/_Elimina"

#: ../src/lib/MainWindow.py:775
msgid "/P_roperties"
msgstr "/Pr_oprietà"

#: ../src/lib/MainWindow.py:863
msgid "Delete subscription?"
msgstr "Annullare sottoscrizione?"

#: ../src/lib/MainWindow.py:864
msgid "Deleting a subscription will remove it from your subscription list."
msgstr ""
"Annullando la sottoscrizione sarà rimossa dalla lista delle sottoscrizioni."

#: ../src/lib/MainWindow.py:1211
msgid "Category error:"
msgstr "Errore categoria:"

#: ../src/lib/MainWindow.py:1216
msgid "Feed error:"
msgstr "Errore feed:"

#: ../src/lib/MainWindow.py:1244
#, python-format
msgid "%(uritems)d unread in %(urfeeds)d feeds"
msgstr "%(uritems)d non letti in %(urfeeds)d feeds"

#: ../src/lib/MainWindow.py:1337
#, python-format
msgid "Next Refresh: %s"
msgstr "Successivo aggiornamento: %s"

#: ../src/lib/MainWindow.py:1383
msgid "Straw is a desktop news aggregator for GNOME"
msgstr "Straw è un aggregatore di news per GNOME"

#: ../src/lib/PollManager.py:192
msgid "No data"
msgstr "Nessun dato"

#: ../src/lib/PollManager.py:196 ../src/lib/SubscribeDialog.py:290
#, python-format
msgid "Unable to find the feed (%s: %s)"
msgstr "Impossibile trovare il feed (%s: %s)"

#: ../src/lib/PollManager.py:199 ../src/lib/PollManager.py:280
#: ../src/lib/SubscribeDialog.py:292
msgid "Invalid username and password."
msgstr "Nome utente e password non validi."

#: ../src/lib/PollManager.py:201 ../src/lib/SubscribeDialog.py:295
#, python-format
msgid "Updating feed resulted in abnormal status '%s' (code %d)"
msgstr ""
"L'aggiornamento del feed ha provocato uno stato anormale «%s» (codice %d)"

#: ../src/lib/PollManager.py:212
#, python-format
msgid "An error occurred while processing feed: %s"
msgstr "Si è verificato un errore durante l'elaborazione del feed: %s"

#: ../src/lib/PollManager.py:216
#, python-format
msgid "Updating %s done."
msgstr "Aggiornamento di %s fatto."

#: ../src/lib/PollManager.py:229
#, python-format
msgid "Updating %s failed"
msgstr "Aggiornamento di %s fallito"

#: ../src/lib/PollManager.py:275 ../src/lib/SubscribeDialog.py:286
#, python-format
msgid "No data (%s)"
msgstr "Nessun dato (%s)"

#: ../src/lib/PollManager.py:277
#, python-format
msgid "Unable to find the category (%s: %s)"
msgstr "Impossibile trovare la categoria (%s: %s)"

#: ../src/lib/PollManager.py:282
#, python-format
msgid "Updating category resulted in abnormal status '%s' (code %d)"
msgstr ""
"L'aggiornamento della categoria ha provocato uno stato anormale «%s» (codice "
"%d)"

#: ../src/lib/PreferencesDialog.py:44
msgid "_Category"
msgstr "_Categoria"

#: ../src/lib/PreferencesDialog.py:143
msgid "Category name"
msgstr "Nome categoria"

#: ../src/lib/PreferencesDialog.py:232
msgid "_Member"
msgstr "_Membro"

#: ../src/lib/PreferencesDialog.py:238
msgid "_Feed"
msgstr "_Feed"

#: ../src/lib/SubscribeDialog.py:176
#, python-format
msgid "Connecting to '%s'"
msgstr "Connessione a «%s»"

#: ../src/lib/SubscribeDialog.py:184
#, python-format
msgid "Enter username and password for '%s'"
msgstr "Inserire nome utente e password per «%s»"

#: ../src/lib/SubscribeDialog.py:201
msgid "Getting feed info ...."
msgstr "Prelievo informazioni sul feed ..."

#: ../src/lib/SubscribeDialog.py:206
#, python-format
msgid "Processing %s of %s feeds"
msgstr "Elaborazione %s di %s feed"

#: ../src/lib/SubscribeDialog.py:259
msgid "Unsupported Protocol"
msgstr "Protocollo non supportato"

#: ../src/lib/SubscribeDialog.py:260
#, python-format
msgid "Subscribing to '%s://' is not yet supported"
msgstr "La sottoscrizione tramite «%s://» non è ancora supportata"

#: ../src/lib/SubscribeDialog.py:344
msgid "Forbidden"
msgstr "Vietato"

#: ../src/lib/Tray.py:57
#, python-format
msgid "%d new items"
msgstr "%d nuovi elementi"

#: ../src/lib/URLFetch.py:124
msgid "Host name lookup failed"
msgstr "Risoluzione del nome dell'host fallita"

#: ../src/lib/URLFetch.py:204
msgid "Maximum download time exceeded"
msgstr "È stato superato il tempo massimo di download"

#: ../src/lib/URLFetch.py:260
msgid "Maximum download file size exceeded"
msgstr "È stata superata la dimensione massima del file"

#: ../src/lib/URLFetch.py:267
msgid "Feed is empty."
msgstr "Il feed è vuoto."

#: ../src/lib/file_selector.py:41
msgid "Export Error"
msgstr "Errore d'esportazione"

#: ../src/lib/file_selector.py:42
#, python-format
msgid "%s"
msgstr "%s"

#: ../src/lib/file_selector.py:50
msgid "Unable to import subscriptions"
msgstr "Impossibile importare le sottoscrizioni"

#: ../src/lib/file_selector.py:51
#, python-format
msgid "Error occurred while reading file: %s"
msgstr "Si è verificato un errore durante la lettura del file: %s"

#: ../src/lib/file_selector.py:60
msgid "Export Subscriptions"
msgstr "Esporta sottoscrizioni"

#: ../src/lib/file_selector.py:62
msgid "Select category to export"
msgstr "Selezionare la categoria da esportare"

#: ../src/lib/file_selector.py:70
msgid "Import Subscriptions"
msgstr "Importa sottoscrizioni"

#: ../src/lib/file_selector.py:72
msgid "Add subscriptions in"
msgstr "Aggiungi sottoscrizioni in"

#: ../src/lib/hig_alert.py:68
msgid "Unable to Poll Feeds"
msgstr "Non è possibile aggiornare i feed"

#: ../src/lib/hig_alert.py:69
msgid "You are currently reading offline. Would you like to go online now?"
msgstr "Attualmente stai leggendo offline. Vuoi andare online ora?"

#. this is here just to make xgettext happy: it should be defined in
#. only one place, and a good one would be MainWindow.py module level.
#. however, we can't access _ there.
#. The format: %A is the full weekday name
#. %B is the full month name
#. %e is the day of the month as a decimal number,
#. without leading zero
#. This should be translated to be suitable for the locale in
#. question, feel free to alter the order and the parameters (they
#. are strftime(3) parameters, the whole string is passed to the
#. function, Straw does no additional interpretation) if you feel
#. it's necessary in the translation file.
#: ../src/lib/utils.py:140
msgid "%A %B %e %H:%M"
msgstr "%A %e %B %Y, %H.%M"

#: ../straw.desktop.in.h:1
msgid "Aggregates newsfeeds and blogs"
msgstr "Aggrega feed di notizie e blog"

#: ../straw.desktop.in.h:2
msgid "Straw Desktop News Aggregator"
msgstr "Aggregatore di news Straw"
